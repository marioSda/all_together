#include <iostream>
#include <map>
#include <set>
#include <deque>
using namespace std;

bool fncomp(char lhs, char rhs)
{
	return lhs < rhs;
}

struct classcomp
{
	bool operator()(const char& lhs, const char& rhs) const
	{
		return lhs < rhs;
	}
};

int main()
{
	map<char, int> first;

	first['a'] = 10;
	first['b'] = 20;
	first['c'] = 30;
	first['d'] = 40;

	map<char, int> second(first.begin(), first.end());
	map<char, int> third(second);
	map<char, int, classcomp> fourth;

//	bool(*fn_pt)(char,char) = fncomp;
//	std::map<char,int,bool(*)(char,char)> fifth (fn_pt);

	cout << first['e'] << endl;
	cout << first.size() << endl;

	first['e'] = 300;
	cout << first['e'] << endl;
	cout << sizeof(first['e']) << endl;

	first['f'] = first['e'];
	cout << &first['e'] << endl;
	cout << &first['f'] << endl;

	cout << first.size() << endl;
	first.erase('f');
	cout << first.size() << endl;

	map<char, int>::iterator it;
	it = first.find('a');
	if (it != first.end())
		first.erase(it);

	cout << first.find('b')->second << endl;

	//////////////////////////////////////////////////////////// funkcje szukajace

	map<char, int> myMap;
	map<char, int>::iterator itLow, itUp;

	myMap['a'] = 10;
	myMap['b'] = 20;
	myMap['c'] = 40;
	myMap['d'] = 40;

	itLow = myMap.lower_bound('a');
	itUp = myMap.upper_bound('c');
	myMap.erase(itLow, itUp);
	cout << myMap.size() << endl;

	////////////////////////////////////////////////////////////// zbior

	int tab[] =
	{ 1, 1, 3, 4, 5 };
	set<int> firstSet(tab, tab + 5);

	for (set<int>::iterator it = firstSet.begin(); it != firstSet.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	cout << firstSet.size() << endl;

	set<int> secondSet(firstSet);

	////////////////////////////////////////////////////////////// insert

	multimap<char, int> multi;

	multi.insert(make_pair('x', 10));
	multi.insert(make_pair('y', 20));
	multi.insert(make_pair('z', 30));
	multi.insert(make_pair('z', 40));

	cout << multi.find('z')->second << endl;
	cout << multi.find('z')->second << endl;
	cout << multi.find('z')->second << endl;
	cout << multi.find('z')->second << endl;
	cout << multi.find('z')->second << endl;
	cout << multi.find('z')->second << endl;// jesli chcemy dostep do wszystkich kopi to korzystamy equal_range !!!!

	/////////////////////////////////////////////////////////////////// deque

	deque<int> myDeque(5, 5);
	myDeque.push_front(6);
	myDeque.push_back(6);
	cout << "myDeque: \n";
	for (deque<int>::iterator it = myDeque.begin(); it != myDeque.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	int tab2[] ={ 1, 2, 3, 4, 5, 6 };
	deque<int> myDeque2(tab2, tab2 + (sizeof(tab2) / sizeof(int)));

	cout << "myDeque2: \n";
	for (deque<int>::iterator it = myDeque2.begin(); it != myDeque2.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;


	return 0;
}
