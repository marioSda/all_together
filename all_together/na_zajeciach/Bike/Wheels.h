#ifndef WHEELS_H_
#define WHEELS_H_

class Wheels
{
public:
	enum WheelSize{small, medium, big};
	enum WheelTire{mountain, road, sport};

	Wheels(WheelSize s = medium, WheelTire t = road);

	WheelSize getSize() const{return size;}
	WheelTire getTire() const{return tire;}
	void setSize(WheelSize siz){this->size = siz;}
	void setTire(WheelTire tir){this->tire = tir;}

private:

	WheelSize size;
	WheelTire tire;
};

#endif
