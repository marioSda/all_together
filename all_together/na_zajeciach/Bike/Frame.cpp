#include "Frame.h"
#include <iostream>
using namespace std;

Frame::Frame(Frame::frameType typ, Frame::frameSize siz) :
type(typ), size(siz)
{
	switch (type)
	{
	case hardtail:
		cout << " " << "-Hardtail type frame chosen... " << endl;
		break;
	case road:
		cout << " " << "-Road type frame chosen... " << endl;
		break;
	case sport:
		cout << " " << "-Sport type frame chosen... " << endl;
		break;
	case light:
		cout << " " << "-Other type frame chosen... " << endl;
		break;
	}

	switch(siz)
	{
	case small:
		cout << " " << "-Small size of frame chosen... " << endl;
		break;
	case medium:
		cout << " " << "-Medium size of frame chosen... " << endl;
		break;
	case large:
		cout << " " << "-Large size of frame chosen... " << endl;
		break;
	}

}

