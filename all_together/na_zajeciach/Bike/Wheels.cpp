#include "Wheels.h"
#include <iostream>
using namespace std;

Wheels::Wheels(Wheels::WheelSize s, Wheels::WheelTire t)
{
	size = s;
	tire = t;

	switch (s)
	{
	case 0:
		cout << " " << "-Small wheels on... " << endl;
		break;
	case 1:
		cout << " " << "-Medium wheels on... " << endl;
		break;
	case 2:
		cout << " " << "-Big wheels on... " << endl;
		break;
	}

	switch (t)
	{
	case 0:
		cout << " " << "-Mountain type tires on... " << endl;
		break;
	case 1:
		cout << " " << "-Road type tires on... " << endl;
		break;
	case 2:
		cout << " " << "-Sport type tires on... " << endl;
		break;
	}
}

