#include "Lights.h"
#include <iostream>
using namespace std;

Lights::Lights(bool front, bool back)
{
	fLight = front;
	bLight = back;
}

void Lights::frontlightOn()
{
	fLight = true;
	cout << "Front lights are on!" << endl;
}
void Lights::frontlightOff()
{
	fLight = false;
	cout << "Front lights are off!" << endl;
}
void Lights::backlightOn()
{
	bLight = true;
	cout << "Back lights are on!" << endl;
}
void Lights::backlightOff()
{
	bLight = false;
	cout << "Back lights are off!" << endl;
}

