#ifndef FRAME_H_
#define FRAME_H_

class Frame
{

public:

	enum frameType{hardtail, road, sport, light};
	enum frameSize{small, medium, large};

	Frame(frameType typ = road, frameSize siz = medium);

	frameSize getSize() const{return size;}
	frameType getType() const{return type;}
	void setSize(frameSize size){this->size = size;}
	void setType(frameType type){this->type = type;}

private:

	frameType type;
	frameSize size;

};

#endif
