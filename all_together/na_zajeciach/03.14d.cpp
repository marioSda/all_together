#include <iostream>
using namespace std;

int main()
{
	int tab[100] =
	{ 1, 2, 3, 4, 5, 6 };
	int pozycja;

	int temp;

	for (int i = 0; i < 6; i++)
	{
		cout << tab[i] << " ";
	}
	cout << endl;
	cout << "Wprowadz indeks wartosci ktora chcesz usunac: " << endl;
	cin >> pozycja;
	if (pozycja > 6)
		cout << "Pozycja wykracza poza zakres " << endl;

	for (int i = pozycja + 1; i <= 6; i++)
	{
		temp = tab[i];
		tab[i - 1] = temp;
	}

	cout << "Nowa tablica" << endl;

	for (int i = 0; i < 6 + 1; i++)
	{
		if (tab[i] != 0)
			cout << tab[i] << " ";
	}

	return 0;
