#include <iostream>
using namespace std;

struct Foo
{
	string random = "random text";
};

int main() {

	const int range = 1000000;


	cout << "Stack Overflow Test" << endl;

//	Foo b[range];
	Foo *a = new Foo[range];
//	static Foo tab[range];

	cout << "Stack Overflow Test ok" << endl;
	return 0;
}
