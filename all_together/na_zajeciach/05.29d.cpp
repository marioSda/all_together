#include <list>
#include <iostream>
using namespace std;

int main()
{

	list<int> first(10);
	list<int> second(10, 5);
	list<int>::iterator it = first.end();

	first.splice(it, second);

	for (list<int>::iterator i = first.begin(); i != first.end(); i++)
	{
		cout << *i << " ";
	}
	cout << endl;

	for (list<int>::iterator i = second.begin(); i != second.end(); i++)
	{
		cout << *i << " ";
	}



	return 0;
}
