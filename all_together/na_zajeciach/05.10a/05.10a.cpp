#include <iostream>
#include "Player.h"
using namespace std;

int random = 2;

void function()
{
	int local = 0;
	static int global = 0;
	std::cout << global++ << " ";
	std::cout << local++ << " ";
	std::cout << random++ << std::endl;
}



int main() {

//	static int global = 0;
//
//	for(int i = 0; i < 5; i++, random++)
//	{
//		function();
//	}

	Player monster;
	Player fighter;
	Player ninja;
	monster.kill();
	monster.kill();
	fighter.kill();
	fighter.kill();
	ninja.kill();
	ninja.kill();
	Player player4 = monster;

	cout <<"Overall killed: " << Player::getOverallKills() << endl;
	cout <<"Total objects created: " << Player::getObjectsCreated() << endl;
	cout <<"Currently working objects: " << Player::getCurrentObjects() << endl;
	monster.restorePlayer(fighter);
	fighter.restorePlayer(ninja);
	cout <<"Overall killed: " << Player::getOverallKills() << endl;
	cout <<"Total objects created: " << Player::getObjectsCreated() << endl;
	cout <<"Currently working objects: " << Player::getCurrentObjects() << endl;





	return 0;
}
