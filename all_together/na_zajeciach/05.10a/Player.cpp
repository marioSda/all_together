#include "Player.h"
#include <iostream>

int Player::OverallKills=0;
int Player::ObjectsCreated=0;
int Player::currentObjects=0;

Player::Player()
{
	ObjectsCreated++;
	currentObjects++;
}

Player::Player(const Player& obj)
{
	currentObjects++;
	ObjectsCreated++;
}

Player::~Player()
{
	currentObjects--;
}

void Player::restorePlayer(Player object)
{
	std::cout << "*** Player restored *** " << std::endl;
}
