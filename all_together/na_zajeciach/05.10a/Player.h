#ifndef PLAYER_H_
#define PLAYER_H_

class Player
{
public:
	Player();
	Player(const Player &obj);
	virtual ~Player();

	void kill(){OverallKills++;}
	void restorePlayer(Player object);

	static int getOverallKills(){return OverallKills;}
	static int getObjectsCreated(){return ObjectsCreated;}	//const oznacza nie zmienianie stanu obiektu.
	static int getCurrentObjects(){return currentObjects;}	// tutaj nie ma sensu wstawiac const bo nie ma obiektu (zmienne typu static bez obiektu)

private:
	static int OverallKills;
	static int ObjectsCreated;
	static int currentObjects;
};

#endif
