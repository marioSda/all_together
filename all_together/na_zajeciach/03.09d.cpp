#include <iostream>
using namespace std;

int main() {

	float liczba;
	float suma=0;
	int licznik = 0;
	bool flaga = true;

	while(flaga)
	{
		cin >> liczba;
		if(liczba!=0 && liczba >=26 && liczba <= 50)
		{
			licznik++;
			suma+=liczba;
		}
		else
			flaga = false;

	}

	float srednia = suma/licznik;

	cout <<"Suma wynosi: " << suma << ", a srednia: " << srednia << endl;

	return 0;
}
