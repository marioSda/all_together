#include "Circle.h"
#include <string>
#include <iostream>

Circle::Circle(std::string n, double r):
    Shape(n)
{
    radius = r;
}

void Circle::draw()
{
    for (int i = -radius; i <= radius; i++)
    {
        for (int j = -radius-5; j <= radius+5; j++)
        {
            float d = (j/radius)*(j/radius) + (i/radius)*(i/radius);
            if (d > 0.0 && d < 0.9)
                std::cout << "*";
            else
                std::cout << " ";
        }
        std::cout << std::endl;
    }
}

void Circle::description()
{
    std::cout << "CIRCLE\n"
              << "=========\n"
              << "-name: "
              << getName() << "\n"
              << "-radius: "
              << getRadius() << "\n"
              << "-area: "
              << area() << "\n"
              << "-example: "
              << std::endl;
    draw();
}




