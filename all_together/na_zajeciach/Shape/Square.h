#ifndef SQUARE_H
#define SQUARE_H

#include "Shape.h"
#include <string>

class Square : public Shape
{
public:
    Square(std::string n, double s=10);
    void draw();
    void description();
    double area(){return side*side;};
    double getSide(){return side;};

private:
    double side;


};

#endif
