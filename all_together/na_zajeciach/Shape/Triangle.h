#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Shape.h"
#include <string>

class Triangle : public Shape
{
public:
    Triangle(std::string n, double b=10, double h=10);
    void draw();
    void description();
    double getBase(){return base;};
    double getHeight(){return height;};
    double area(){return base*height*0.5;};

private:
    double base;
    double height;


};

#endif
