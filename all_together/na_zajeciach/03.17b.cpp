#include <iostream>
#include <cmath>
#include <cstdlib>
#include <time.h>
#include <iomanip>
//// MERGE_SORT ////
using namespace std;

const int N = 20;

int main()
{
    int d[N], i, j, temp;

    cout<<"Sortowanie przez wstawianie "<<endl;

    // wypelniamy tablice liczbami pseudolosowymi
    // a nastepnie wyswietlamy jej zawartosc

    srand((int)time(NULL));

    for(i = 0; i < N; i++) d[i] = rand() % 100;
    for(i = 0; i < N; i++) cout << setw(5) << d[i];
    cout << endl;

    // sortujemy

    for(j = N - 2; j >= 0; j--)
    {
        temp = d[j];
        i = j + 1;
        while((i < N)&&(temp > d[i]))
        {
            d[i-1 ] = d[i];
            i++;
        }
        d[i-1] = temp;
    }

    // Wynik sortowania
    cout << "Po sortowaniu:\n";
    for(i = 0; i < N; i++) cout << setw(5) << d[i];

    return 0;
}
