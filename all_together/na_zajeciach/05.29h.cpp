#include <iostream>
#include <list>

using namespace std;

int main()
{
	int tab[] = {1,2,3,4,5,6,7,8,9};
	list<int> myList(tab, tab+9);

	for (list<int>::iterator it = myList.begin(); it != myList.end(); it++)
	{
		cout << *it << " ";
	}

	int start;
	int end;
	cout << "\nWhat range would you like to delete? " << endl;
	cin >> start >> end;
	start -= 1;
	end -= 1;

	list<int>::iterator i = myList.begin();
	list<int>::iterator j = myList.begin();
	advance(i,start);
	advance(j, end+1);
	myList.erase(i, j);

	for (list<int>::iterator it = myList.begin(); it != myList.end(); it++)
	{
		cout << *it << " ";
	}

	return 0;
}
