
#ifndef SUFFIX_H_
#define SUFFIX_H_

#include <string>

class Suffix
{
public:
	Suffix();
	virtual ~Suffix();
	void setText(std::string value);
	int getCounter(){return counter;}

private:
	int counter;
	std::string text;
};

#endif
