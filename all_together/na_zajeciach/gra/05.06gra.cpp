#include <iostream>
#include "KeyManager.h"
#include "Map.h"
#include "MapDisplayer.h"
#include "GameLogic.h"

using namespace std;

void printHelp()
{
	cout
			<< "Help: \no - exit\nr - idle\nw - up\na - left\ns - down\nd - right\n"
			<< endl;
}

int main()
{

	KeyManager *key_manager = new KeyManager; // utowrzenie obiektu key menager
	GameLogic *game_logic = new GameLogic; // utworzenie obiektu logiki
	Map *map = new Map; // utworzenie obiektu mapy
	MapDisplayer *map_displayer = new MapDisplayer; // utowrzenie obiektu wyswietlajacego mape

	map_displayer->setMap(map);
	map->setObjectList(game_logic->getObjectList());
	game_logic->setKeyManager(key_manager);
	game_logic->configureGame();
	map->generateMap();
	map_displayer->displayMap();


	bool exit = false;

	while (exit == false)
	{
		// sprawdzic czy gra jest skonfigurowana
		// popros o kolejny znak
		// wczytamy kolejny znak
		char input;
		cout << "Please enter a key and press enter (h for help)" << endl;
		cin >> input;
		if (input == 'h')
		{
			printHelp();
			continue;
		}

		key_manager->addNewKey(input);	// wczytujemy koleny znak

		if (game_logic->nextTurn())		// Logika gry
		{
			map->generateMap();	// generowanie mapy
			map_displayer->displayMap();	// wyswietlanie mapy
			exit = game_logic->nextTurn();
			cout << "Gotcha! Game Over!!!" << endl;
			continue;
		}
		map->generateMap();
		map_displayer->displayMap();
	}

	delete key_manager; // usuniece obiektu key menager
	delete game_logic; // usuniece obiektu logiki
	delete map; // usuniece obiektu mapy
	delete map_displayer; // usuniece obiektu wyswietlajacego mape
	cout << "Game is ending..." << endl;

	return 0;
}
