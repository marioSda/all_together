#ifndef PLAYER_H_
#define PLAYER_H_

#include "GameObject.h"

class Player: public GameObject
{
public:
	Player();
	virtual ~Player();

	char getSymbol() const{return '@';}

};

#endif

