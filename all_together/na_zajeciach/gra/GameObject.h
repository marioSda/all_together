#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

class GameObject
{
public:
	GameObject();
	virtual ~GameObject();

	int getX() const{return x;}
	int getY() const{return y;}

	virtual char getSymbol() const{return 'o';}

	void setX(int x){this->x = x;}
	void setY(int y){this->y = y;}

private:

	int x, y;
};

#endif
