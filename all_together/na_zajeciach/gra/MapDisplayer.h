#ifndef MAPDISPLAYER_H_
#define MAPDISPLAYER_H_
#include "Map.h"

class MapDisplayer
{
public:
	MapDisplayer();
	virtual ~MapDisplayer();
	void setMap(Map *_pMap);
	void displayMap();

private:
	Map *pMap;
};

#endif
