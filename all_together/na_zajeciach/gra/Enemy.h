#ifndef ENEMY_H_
#define ENEMY_H_

#include "GameObject.h"

class Enemy: public GameObject
{
public:
	Enemy();
	virtual ~Enemy();

	char getSymbol() const{return 'X';}
};

#endif
