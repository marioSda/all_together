#include "MapDisplayer.h"
#include <iostream>

MapDisplayer::MapDisplayer()
{
	pMap = nullptr;
}

MapDisplayer::~MapDisplayer()
{
	// TODO Auto-generated destructor stub
}

void MapDisplayer::setMap(Map* _pMap)
{
	pMap = _pMap;

}

void MapDisplayer::displayMap()
{
	if(pMap==nullptr)
		return;


	for(int i = 0; i < 25; i++)
		{
			for(int j = 0; j < 25; j++)
			{
				std::cout << pMap->getPoint(j,i) << " ";
			}
			std::cout << std::endl;
		}
}
