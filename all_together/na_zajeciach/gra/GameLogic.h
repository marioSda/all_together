#ifndef GAMELOGIC_H_
#define GAMELOGIC_H_

#include "Player.h"
#include "Enemy.h"
#include "KeyManager.h"

class GameLogic
{
public:
	GameLogic();
	virtual ~GameLogic();
	void configureGame();
	bool nextTurn(); // zwraca info na temat konca gry;
	void randomPosition(GameObject *object);
	bool checkPosition(int x, int y);
	bool checkPlayerPosiotion();

	GameObject** getObjectList() {return object_list;}
	GameObject* getObjectList(int index){return object_list[index];}

	void setKeyManager(KeyManager* keyManager){key_manager = keyManager;}



private:
	void updatePlayerPosition();
	KeyManager *key_manager;
	Player *player;
	Enemy *enemies[5];
	GameObject *object_list[6];
};

#endif
