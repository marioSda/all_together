#ifndef KEYMANAGER_H_
#define KEYMANAGER_H_

class KeyManager
{
public:
	KeyManager();
	virtual ~KeyManager();
	void addNewKey(char _key);
	char getNextKey(){return key;}

private:
	char key;


};

#endif
