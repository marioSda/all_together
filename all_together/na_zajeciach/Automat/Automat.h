#ifndef AUTOMAT_H_
#define AUTOMAT_H_
#include <string>

const int MAXproducts = 20;

struct Product
{
	std::string brand;
	int price;
};

class Automat
{
public:

	Automat();
	virtual ~Automat();

	void showWhatsInAutomat();
	std::string showSingleProduct(int index);
	void chooseProduct(int index);
	Product getLastChosen();
	float putAmount(float amount);
	float cancelPurchase();

	void addProductToAutomat(std::string name, float price);
	void fillAutomatWithProducts();

private:


	Product products[MAXproducts];
	Product chosenProduct;
	int productCounter;
	float price;
};

#endif
