#include "Automat.h"
#include <string>
#include <iostream>
#include <cmath>

Automat::Automat()
{
	products[0] = {"snickers ", 180};
	products[1] = {"mars ", 170};
	products[2] = {"M&M ", 190};
	products[3] = {"sparkling water ", 150};
	products[4] = {"still water ", 150};
	products[5] = {"orange juice ", 200};
	products[6] = {"bread roll ", 250};
	productCounter = 7;
	chosenProduct = {"",0};
	price = 0;
}

Automat::~Automat()
{


}

std::string Automat::showSingleProduct(int index)
{
	std::string singleProduct;

	if(index >=0 && index <= productCounter)
	{
		singleProduct = products[index].brand;
		singleProduct += products[index].price;
	}
		return singleProduct;
	return "";
}



void Automat::showWhatsInAutomat()
{
	for(int i = 0; i < productCounter; i++)
	{
		std::cout << showSingleProduct(i) << std::endl;
	}
}

void Automat::chooseProduct(int index)
{
	if(index >=0 && index <= productCounter)
	{
		chosenProduct = products[index];
	}
}

Product Automat::getLastChosen()
{
	return chosenProduct;
}

float Automat::putAmount(float amount)
{
	price = chosenProduct.price;
	float excess;

	if(price!=0)
	{
		if(chosenProduct.price-=amount>=0)
		{
			price -= amount;

		if(price == 0)
			return 0;
		else if(price < 0)
		{
			excess = price;
			price = 0;
			return std::abs(excess);
		}
		return -price;
		}
	}
	return 1234;
}

float Automat::cancelPurchase()
{
	float excess;

	if(price != 0 && price < chosenProduct.price)
	{
		excess = price;
		price = 0;
		return excess;
	}

	chosenProduct = {"",0};
	price = 0;
	return 0;
}

void Automat::addProductToAutomat(std::string name, float price)
{
	products[productCounter] = {name, price};
	productCounter++;
}

void Automat::fillAutomatWithProducts()
{
	for(int i = 0; i < MAXproducts; i++)
	{
		products[i] = {"random "+(i+1), 1+i*0.10};
	}
}
