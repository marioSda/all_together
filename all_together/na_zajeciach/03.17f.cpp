#include <iostream>
using namespace std;
///// dodawanie, odejmowanie, mnozenie macierzy /////
void suma(int tab[][3], int tab2[][3], int r);
void roznica(int tab[][3], int tab2[][3], int r);
void mnozenie(int tab[][3], int tab2[][3], int r);

int main() {

	int tab1[3][3]={{1,2,3},
					{4,5,6},
					{7,8,9}};
	int tab2[3][3]={{2,2,2},
					{2,2,2},
					{2,2,2}};

	suma(tab1,tab2,3);
	roznica(tab1,tab2,3);
	mnozenie(tab1,tab2,3);

	return 0;
}

void suma(int tab[][3], int tab2[][3], int r)
{
	cout<<"suma macierzy: "<<endl;
	int tab3[r][3];

	for(int i = 0; i < r; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			tab3[i][j] = tab[i][j]+tab2[i][j];
		}
	}

	for(int i = 0; i < r; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				cout << tab3[i][j] << " ";
			}
			cout << endl;
		}
	cout<<endl;

}

void roznica(int tab[][3], int tab2[][3], int r)
{
	cout<<"roznica macierzy: "<<endl;
	int tab3[r][3];

	for(int i = 0; i < r; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			tab3[i][j] = tab[i][j]-tab2[i][j];
		}
	}

	for(int i = 0; i < r; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				cout << tab3[i][j] << " ";
			}
			cout << endl;
		}
	cout<<endl;
}

void mnozenie(int tab[][3], int tab2[][3], int r)
{
	cout<<"mnozenie macierzy: "<<endl;
	int tab3[r][3];

	for(int i = 0; i < r; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			tab3[i][j] = tab[i][j]*tab2[i][j];
		}
	}

	for(int i = 0; i < r; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				cout << tab3[i][j] << " ";
			}
			cout << endl;
		}
	cout<<endl;
}

