#include <iostream>
#include <list>

using namespace std;
int main()
{
	int tab[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
	list<int> first(tab, tab + 15);

	list<int> odd(0);
	list<int> even(0);

	for (list<int>::iterator i = first.begin(); i != first.end(); i++)
	{
		if (*i % 2 == 0)
			even.push_back(*i);
		else
			odd.push_back(*i);
	}

	first.erase(first.begin(), first.end());
	list<int>::iterator it = first.begin();
	first.splice(it,odd);
	even.reverse();
	first.splice(it, even);

	for (list<int>::iterator i = first.begin(); i != first.end(); i++)
	{
		cout << *i << " ";
	}

	return 0;
}
