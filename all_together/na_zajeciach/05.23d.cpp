#include <iostream>
using namespace std;


int solution(int A, int B)
{
	unsigned long long int A2 = A;
	unsigned long long int value = A2*B;
	int counter = 0;

	while(value)
	{
		if(value%2 ==1)
		{
			counter++;
		}
		value /= 2;
	}

	return counter;
}

int main() {

		int two = 100000000;
	    int three = 100000000;
	    long long int two2 = two;
		unsigned long long int result = two2 * three;

		cout << result << endl;
		cout << solution(two, three) << endl;


	return 0;
}
