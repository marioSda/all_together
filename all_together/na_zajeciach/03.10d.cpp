#include <iostream>
using namespace std;

int fib(int numer);

int main() {

    int liczba;
    cout << "Podaj numer liczby z ciagu Fibonacciego "<< endl; cin >> liczba;
    cout<<fib(liczba)<<endl;

    return 0;
}

int fib(int numer)
{
    if(numer < 3)
        return 1;
    else
        return fib(numer-2)+fib(numer-1);
}
