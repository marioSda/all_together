#include <vector>
#include <iostream>
using namespace std;

int main()
{

	vector<int> first;
	vector<int> second(4, 100);
	vector<int> third(second.begin(), second.end());
	vector<int> fourth(third);

	int myInts[] =
	{ 1, 2, 3, 4 };

	vector<int> fifth(myInts, myInts + 4);

	cout << "The content of fifth is: " << endl;

	for (vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)
	{
		cout << *it << " ";
	}

	cout << endl;

	for (unsigned int i = 0; i < fourth.size(); i++)
	{
		cout << fourth[i] << " ";
	}

	cout << endl;

	for (vector<int>::iterator it = third.begin(); it != third.end(); it++)
	{
		cout << *it << " ";
	}

	for (vector<int>::iterator it = first.begin(); it != first.end(); it++)
	{
		cout << *it << " ";
	}

	return 0;
}
