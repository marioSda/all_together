#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <time.h>
//// QUICK_SORT ////
using namespace std;

const int N = 20; // liczebnosc zbioru
int d[N];

void quickSort(int lewy, int prawy)
{
    int i,j,piwot;

    i = (lewy+prawy)/2;
    piwot = d[i];
    d[i] = d[prawy];
    for(j = i = lewy; i < prawy; i++)
    {
        if(d[i] < piwot)
        {
            swap(d[i], d[j]);
            j++;
        }
    }
    d[prawy] = d[j];
    d[j] = piwot;
    if(lewy < j - 1) quickSort(lewy, j-1);
    if(j + 1 < prawy) quickSort(j+1, prawy);
}

int main()
{
    int i;

    srand((int)time(NULL));

    for(i = 0; i < N; i++) d[i] = rand()% 100;
    for(i = 0; i < N; i++) cout << d[i] << " ";
    cout << endl;

    quickSort(0, N-1);

    cout << "po sortowaniu " << endl;
    for(i = 0; i < N; i++) cout << d[i] << " ";
    cout << endl;
    return 0;
}
