#include <iostream>
using namespace std;

int factorial(int digit)
{
	if(digit < 2)
		return 1;
	else
		return digit*factorial(digit-1);
}

int factorialLoop(int digit)
{
	int total=1;
	for(int i =1; i <= digit ; i++)
	{
		total *= i;
	}
	return total;
}

int main() {

	cout << factorial(8) << endl;
	cout << factorialLoop(8) << endl;

	return 0;
}
