#include <iostream>
using namespace std;
//////////// Wprowadzenie liczb i wyswietlenie ich na ekran ///////////////
int main()
{
	const long int max_size = 10;
	cout << "Podaj liczby: " << endl;
	int t[max_size] ={};
	int temp;

	for (int i = 0; i < max_size; ++i)
	{
		cin >> temp;

		if (temp != 0)
		{
			t[i] = temp;
		}
		else
			break;
	}

	for (int j = 0; j < max_size; ++j)
	{
		cout << t[j] << " ";
	}

	return 0;
}
