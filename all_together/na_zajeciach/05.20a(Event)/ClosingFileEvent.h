#ifndef CLOSINGFILEEVENT_H_
#define CLOSINGFILEEVENT_H_

#include <string.h>
#include "Event.h"

class ClosingFileEvent:public Event
{
public:
	ClosingFileEvent();
	virtual ~ClosingFileEvent();

	std::string getEventType();
	std::string getOperationName();

private:
	static const std::string eventType;
	std::string operationName;
};

#endif
