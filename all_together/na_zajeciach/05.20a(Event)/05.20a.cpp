#include <iostream>
#include "Event.h"
#include "OpenFileEvent.h"
#include "ClosingFileEvent.h"

using namespace std;

void doSomething(int* A, int size_A)
{
	for(int i = 0; i < size_A; ++i)
	{
		std::cout << A[i] << "\n";
	}
}


void displayEvent(Event *event)
{
	std::cout << event->getEventType() << endl;
	if(event->getEventType() == "OpenFileEvent")
	{
		OpenFileEvent *openEvent = static_cast<OpenFileEvent*>(event);		// wykonywane w czasie kompilacji	// static_cast jest bez kosztowny co do czasu dzialania
		std::cout << "trying to open file: " << openEvent->getFileName();
	}else if(event->getEventType() == "closingTime")
	{
		ClosingFileEvent *closingEvent = static_cast<ClosingFileEvent*>(event);
		std:: cout << "trying to open file: " << closingEvent->getOperationName();
	}

	OpenFileEvent *openEvent = dynamic_cast<OpenFileEvent*>(event);	// w czasie wykonywania programu
	if(openEvent)
	{
		std::cout << "Got an openEvent!";
	}
	std::cout << "\n";
}

int main() {

	Event gev = Event();
	OpenFileEvent oev("a.txt");
	Event *ev = &gev;
	ClosingFileEvent cl;
	displayEvent(ev);
	ev = &oev;
	displayEvent(ev);
	ev = &cl;
	displayEvent(ev);


	return 0;
}
