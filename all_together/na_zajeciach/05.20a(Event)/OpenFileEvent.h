#ifndef OPENFILEEVENT_H_
#define OPENFILEEVENT_H_

#include <iostream>
#include "Event.h"

class OpenFileEvent: public Event
{
public:
	OpenFileEvent(std::string fileToOpen);
	virtual ~OpenFileEvent();
	std::string getEventType();
	std::string getFileName();

private:
	static const std::string eventType;
	std::string fileName;
};

#endif
