#ifndef EVENT_H_
#define EVENT_H_

#include <iostream>
#include <string.h>

class Event
{
public:
	Event();
	virtual ~Event();
	virtual std::string getEventType();

protected:
	static const std::string eventType;
};

#endif
