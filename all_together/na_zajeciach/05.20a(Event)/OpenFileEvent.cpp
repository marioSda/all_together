#include "OpenFileEvent.h"

const std::string OpenFileEvent::eventType = "OpenFileEventType";

OpenFileEvent::OpenFileEvent(std::string fileToOpen)	// konstruktor kopiujacy
{
	fileName = fileToOpen;		// najpierw konstruktor domyslny a pozniej operator przypisania
}

OpenFileEvent::~OpenFileEvent()
{

}

std::string OpenFileEvent::getEventType()
{
	static std::string eventType = "OpenFileEvent";
	return eventType;
}

std::string OpenFileEvent::getFileName()
{
	return fileName;
}
