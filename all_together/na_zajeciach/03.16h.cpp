#include <iostream>
using namespace std;
//// operowanie na tablicy w funkcjach z wykorzystaniem wskaznikow ////////
void wyswietl(int *s)
{
	while (*s < 6)
	{
		cout << *s << endl;
		s++;
	}

}

void zmien(int *s)
{
	while(*s<6)
	{
		*s-=1;
		s++;
	}

}

int main()
{
	int tab[5] = { 1, 2, 3, 4, 5 };
	int *wsk = tab;
	zmien(wsk);

	wyswietl(wsk);



	return 0;
}
