#include <iostream>
#include <list>
using namespace std;


bool divided_six(const int& value)
{
	if(value%6==0)
		return true;
	return false;
}

int main()
{
	int tab[] =
	{ 1, 2, 3, 4, 5, 6, 7, 8 };

	list<int> random(tab, tab + 8);

	random.push_back(1);

	for (list<int>::iterator it = random.begin(); it != random.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	random.reverse();

	for (list<int>::iterator it = random.begin(); it != random.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	random.sort();
	for (list<int>::iterator it = random.begin(); it != random.end(); it++)
	{
		cout << *it << " ";
	}

	cout << "\nnew tests: " << endl;

	int tab2[] = {6,5,4,3,1,1};
	list<int> another(tab2, tab2+5);

	another.unique();

	for (list<int>::iterator it = another.begin(); it != another.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	another.remove(5);

	cout << "remove" << endl;
	for (list<int>::iterator it = another.begin(); it != another.end(); it++)
	{
		cout  << *it << " ";
	}
	cout << endl;
	another.remove_if(divided_six);

	for (list<int>::iterator it = another.begin(); it != another.end(); it++)
		{
			cout  << *it << " ";
		}

	return 0;
}
