#include "Course.h"
#include <string>
#include <iostream>
#include "Navigation.h"

Course::Course()
    {
    licznik = 0;
    licznik2 = 0;
    licznik3 = 0;
    places = 0;

    }

Course::Course(std::string stDate, std::string finDate, std::string top,
	int plc)
    {
    startDate = stDate;
    finishDate = finDate;
    topic = top;
    places = plc;
    licznik = 0;
    licznik2 = 0;
    licznik3 = 0;

    }

void Course::description()
    {
    std::cout << std::endl;
    std::cout << "Course: " << topic << "\n" << "Start date: " << startDate
	    << "\n" << "Finish date: " << finishDate << "\n" << "Places: "
	    << places << "\n" << "Vacant plc: " << places - licznik << "\n"
	    << std::endl;
    }

void Course::enrolStudent(Student* stud)
    {
    students[licznik] = stud;
    licznik++;
    stud->assignToCourse(this);
    }

void Course::enrolLecturer(Lecturer* lect)
    {
    lecturers[licznik2] = lect;
    licznik2++;
    lect->addCourse(this);
    }

void Course::assignRoom(Room* room)
    {
    roomForCourse[licznik3] = room;
    licznik3++;
    room->addCourse(this);
    }

void Course::getLecturer()
    {
    std::cout << "Lecturers providing course " << topic << " : \n" << std::endl;
    for (int i = 0; i < licznik2; i++)
	{
	std::cout << i + 1 << " - ";
	lecturers[i]->description();
	}
    }

void Course::getStudents()
    {
    std::cout << "Students enrolled to course " << topic << " : \n"
	    << std::endl;
    for (int i = 0; i < licznik; i++)
	{
	std::cout << i + 1 << " - ";
	students[i]->description();
	}
    }

void Course::getRooms()
    {
    std::cout << "Rooms assigned to course " << topic << " : \n" << std::endl;
    for (int i = 0; i < licznik3; i++)
	{
	std::cout << i + 1 << " - ";
	roomForCourse[i]->description();
	}
    }

