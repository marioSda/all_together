#include "Room.h"
#include <iostream>
#include <string>

Room::Room()
    {
    wsk = 0;
    number = 0;
    capacity = 0;
    building = 0;
    }

Room::Room(int no, int cap, int b)
    {
    wsk = 0;

    number = no;
    capacity = cap;
    building = b;
    }

void Room::description()
    {
    std::cout << std::endl;
    std::cout << "Room: " << number << "\n" << "Capacity: " << capacity << "\n"
	    << "Building: " << building << "\n" << "Courses: " << wsk
	    << std::endl;

    for (int i = 0; i < wsk; i++)
	{
	std::cout << i + 1 << "-" << courses[i]->getTopic() << std::endl;
	}
    }

void Room::addCourse(Course* course)
    {
    if (wsk == 10)
	std::cout << "No more courses can be added!" << std::endl;

    while (wsk != size)
	{
	courses[wsk] = course;
	wsk++;
	break;
	}
    }

void Room::coursesInRoom()
    {
    std::cout << "Room: " << number << ", number of courses: " << wsk
	    << std::endl;
    for (int i = 0; i < wsk; i++)
	{
	std::cout << i + 1 << " - " << courses[i] << std::endl;
	}
    }

