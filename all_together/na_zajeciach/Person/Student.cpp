#include "Student.h"
#include <iostream>
#include <string>
#include "Course.h"

Student::Student()
    {
    wsk2 = 0;
    }

Student::Student(std::string sNam, std::string sSur, int sAge,
	std::string sMail) :
	Person(sNam, sSur, sAge, sMail)
    {
    wsk2 = 0;
    }

void Student::description()
    {
    std::cout << "student: " << getName() << " " << getSurname() << "\n"
	    << "age: " << getAge() << "\n" << "mail: " << getMail() << "\n"
	    << "courses assigned: " << wsk2 << "\n" << std::endl;
    }

void Student::coursesAttended()
    {
    std::cout << "Student " << getName() << " " << getSurname()
	    << ", has assigned courses: " << wsk2 << std::endl;

    for (int i = 0; i < wsk2; i++)
	{
	std::cout << i + 1 << " - ";
	studentCourses[i]->description();
	std::cout << std::endl;
	}
    }

void Student::assignToCourse(Course* course)
    {
    if (wsk2 == 10)
	std::cout << "No more courses can be added!" << std::endl;

    while (wsk2 != size2)
	{
	studentCourses[wsk2] = course;
	wsk2++;
	break;
	}
    }

