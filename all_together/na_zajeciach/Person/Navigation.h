#ifndef NAVIGATION_H_
#define NAVIGATION_H_
#include <string>
#include "Room.h"
#include "Lecturer.h"
#include "Student.h"
#include "Course.h"

const int licznikStudent = 12;
const int licznikRoom = 4;
const int licznikLecturer = 2;
const int licznikCourse = 10;

class Navigation
{
public:
	Navigation();
	void menu();
	void createCourse(std::string startDate, std::string endDate, std::string topic,
			int vacancies);
	void addStudent(std::string name, std::string surname, int age,
			std::string mail);
	void addLecturer(std::string name, std::string surname, int age,
			std::string mail);
	void addRoom(int number, int capacity, int building);

	void addRandomCourse();
	void addRandomStudent();
	void addRandomLecturer();
	void addRandomRoom();
	void showRandomCourseList();

	void showDetails();

	Lecturer getLecturer(int x){return choiceLecturer[x];}
	Student getStudent(int x){return choiceStudent[x];}
	Room getRoom(int x){return choiceRoom[x];}

private:

	void generateCourses();
	void generateStudent();
	void generateLecturer();
	void generateRoom();

	Course course1;
	Student tab[licznikStudent];
	Room tab2[licznikRoom];
	Lecturer tab3[licznikLecturer];
	Course tab4[licznikCourse];
	Student choiceStudent[100];
	int studentCounter;
	Lecturer choiceLecturer[100];
	int lecturerCounter;
	Room choiceRoom[10];
	int roomCounter;
	Student s1;
	Course courses[100];
	int courseCounter;
	int licznik1;
	int licznik2;
	int licznik3;
};

#endif
