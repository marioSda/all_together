#ifndef COURSE_H_
#define COURSE_H_

#include "Student.h"
#include "Lecturer.h"
#include "Room.h"
#include <iostream>
#include <string>

class Lecturer;
class Room;

int const max_rooms = 5;
int const max_lecturers = 2;
int const max_students = 50;

class Course
{
public:
	Course();
	Course(std::string stDate, std::string finDate, std::string top, int plc);

	void description();
	void enrolStudent(Student* stud);
	void enrolLecturer(Lecturer* lect);
	void assignRoom(Room* room);

	void getLecturer();
	void getStudents();
	void getRooms();

	void setStartDate(std::string stDate){startDate = stDate;}
	void setFinishDate(std::string finDate){finishDate = finDate;}
	void setTopic(std::string top){topic = top;}
	void setPositions(int pos){places = pos;}


	const std::string getStartDate() const{return startDate;}
	const std::string getFinishDate() const{return startDate;}
	const std::string getTopic() const{return topic;}
	int getPlaces() const{return places;}
	int getLecturersAmount()const{return max_lecturers;}
	int getRoomsAmount()const{return max_rooms;}

private:


	Room* roomForCourse[max_rooms];
	Lecturer* lecturers[max_lecturers];
	Student* students[max_students];

	std::string startDate;
	std::string finishDate;
	std::string topic;
	int places;
	int licznik;
	int licznik2;
	int licznik3;

};

#endif
