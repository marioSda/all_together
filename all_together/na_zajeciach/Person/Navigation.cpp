#include "Navigation.h"
#include "Person.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <string>

Navigation::Navigation()
    {
    srand(time(NULL));
    studentCounter = 0;
    lecturerCounter = 0;
    roomCounter = 0;
    courseCounter = 0;

    licznik1 = 0;
    licznik2 = 0;
    licznik3 = 0;

    generateCourses();
    generateLecturer();
    generateStudent();
    generateRoom();
    menu();
    }

void Navigation::menu()
    {
    bool flag = true;
    int choice;
    std::string data;
    std::string surname;
    std::string mail;
    int intData;
    int intData2;
    int intData3;

    while (flag)
	{

	std::cout << "\t\tCourse_Maze ver 1.0.3 \n\n";
	std::cout << "			*** MENU ***			\n" << " 1 - list of courses \n"
		<< " 2 - create your own course \n"
		<< " 3 - enroll yourself to created course \n"
		<< " 4 - create a student an enroll him or her to created course \n"
		<< " 5 - enroll someone random to created course \n"
		<< " 6 - enroll a random lecturer to created course \n"
		<< " 7 - create your own lecturer and enroll him or her to created course \n"
		<< " 8 - assign existing random room to existing course \n"
		<< " 9 - create your own room and assign it to existing course \n"
		<< " 10 - display all your progress \n"
		<< " 11 - exit program \n" << std::endl;

	std::cin >> choice;
	std::cin.ignore();

	switch (choice)
	    {
	case 1:
	    std::cout << "list of courses :" << std::endl;
	    showRandomCourseList();
	    break;
	case 2:
	    std::cout << "Give a starting date: ";
	    std::getline(std::cin, data);
	    std::cin.clear();
	    course1.setStartDate(data);
	    std::cout << "Give a ending gate: ";
	    std::getline(std::cin, data);
	    std::cin.clear();
	    course1.setFinishDate(data);
	    std::cout << "Give a topic of a course: ";
	    std::getline(std::cin, data);
	    std::cin.clear();
	    course1.setTopic(data);
	    std::cout << "Give how many vacancies: ";
	    std::cin >> intData;
	    course1.setPositions(intData);
	    std::cout << "\tSuccess ! \n";
	    course1.description();
	    break;
	case 3:
	    std::cout << "Enter your name: ";
	    std::getline(std::cin, data);
	    std::cin.clear();
	    std::cout << "Enter your surname: ";
	    std::getline(std::cin, surname);
	    std::cin.clear();
	    std::cout << "Enter your age: ";
	    std::cin >> intData;
	    std::cin.clear();
	    std::cout << "Enter your mail address: ";
	    std::cin.ignore();
	    std::getline(std::cin, mail);
	    std::cin.clear();
	    addStudent(data, surname, intData, mail);

	    std::cout << "\tSuccess ! \n";
	    course1.getStudents();
	    break;
	case 4:
	    std::cout << "Enter a student's name: ";
	    std::getline(std::cin, data);
	    std::cin.clear();
	    std::cout << "Enter a student's surname: ";
	    std::getline(std::cin, surname);
	    std::cin.clear();
	    std::cout << "Enter a student's age: ";
	    std::cin >> intData;
	    std::cin.clear();
	    std::cout << "Enter a student's mail address: ";
	    std::getline(std::cin, mail);
	    std::cin.clear();
	    addStudent(data, surname, intData, mail);

	    std::cout << "\tSuccess ! \n";
	    course1.getStudents();
	    break;
	case 5:
	    addRandomStudent();
	    course1.getStudents();
	    break;
	case 6:
	    addRandomLecturer();
	    course1.getLecturer();
	    break;
	case 7:
	    std::cout << "Enter a lecturer's name: ";
	    std::getline(std::cin, data);
	    std::cin.clear();
	    std::cout << "Enter a lecturer's surname: ";
	    std::getline(std::cin, surname);
	    std::cin.clear();
	    std::cout << "Enter a lecturer's age: ";
	    std::cin >> intData;
	    std::cin.clear();
	    std::cout << "Enter a lecturer's mail address: ";
	    std::getline(std::cin, mail);
	    std::cin.clear();
	    addStudent(data, surname, intData, mail);

	    std::cout << "\tSuccess ! \n";
	    course1.getLecturer();
	    break;
	case 8:
	    addRandomRoom();
	    course1.getRooms();
	    break;
	case 9:
	    std::cout << "Enter number of a room ";
	    std::cin >> intData;
	    std::cin.clear();
	    std::cout << "Enter capacity of a room ";
	    std::cin >> intData2;
	    std::cin.clear();
	    std::cout << "Enter number of a room's building ";
	    std::cin >> intData3;
	    std::cin.clear();
	    addRoom(intData, intData2, intData3);
	    std::cout << "\tSuccess ! \n";
	    course1.getRooms();
	    break;
	case 10:
	    showDetails();
	    break;
	case 11:
	    std::cout << "Ending program... " << std::endl;
	    flag = false;
	    break;
	    }
	}
    }

void Navigation::createCourse(std::string startDate, std::string endDate,
	std::string topic, int vacancies)
    {
    std::cout << "Course about " << topic << " for " << vacancies
	    << " people was created." << std::endl;

    course1.setStartDate(startDate);
    course1.setFinishDate(endDate);
    course1.setTopic(topic);
    course1.setPositions(vacancies);

    generateStudent();
    generateLecturer();
    generateRoom();
    }

void Navigation::generateCourses()
    {
    tab4[0] = Course("01.02.2018", "31.03.2018", "Biology", 25);
    tab4[1] = Course("01.04.2018", "30.04.2018", "Mathematics", 20);
    tab4[2] = Course("01.05.2018", "23.05.2018", "Geology", 30);
    tab4[3] = Course("01.06.2018", "30.06.2018", "Physics", 35);
    tab4[4] = Course("01.07.2018", "15.07.2018", "History", 25);
    tab4[5] = Course("01.09.2018", "20.12.2018", "Programming", 27);
    tab4[6] = Course("15.06.2018", "30.08.2018", "Surfing", 10);
    tab4[7] = Course("15.03.2018", "20.04.2018", "Meditation", 20);
    tab4[8] = Course("07.09.2018", "12.12.2018", "Cooking", 10);
    tab4[9] = Course("01.12.2018", "15.12.2018", "Acting", 25);
    }

void Navigation::generateRoom()
    {
    tab2[0] = Room(1, 100, 5);
    tab2[1] = Room(2, 100, 5);
    tab2[2] = Room(3, 100, 5);
    tab2[3] = Room(4, 100, 5);
    }

void Navigation::generateStudent()
    {
    tab[0] = Student("Mariusz", "Swietojanski", 28, "ms@gmail.com");
    tab[1] = Student("msm", "sad", 27, "ads@o.pl");
    tab[2] = Student("Kuba", "Ktos", 30, "mama@o3.pl");
    tab[3] = Student("Tomek", "Tomaszewski", 22, "ows@o2.pl");
    tab[4] = Student("Wojtek", "Swiatek", 34, "oe2@ow.pl");
    tab[5] = Student("Bartek", "Dajwor", 33, "9029@op.pl");
    tab[6] = Student("Czesiek", "Traba", 28, "czech@o2.pl");
    tab[7] = Student("Tobiasz", "Serek", 29, "ts@o2.pl");
    tab[8] = Student("Krzysiek", "Krzych", 30, "krk@o2.pl");
    tab[9] = Student("Blazej", "Wojcieszek", 22, "bw@o2.pl");
    tab[10] = Student("Ireneusz", "Krak", 34, "irek@ow.pl");
    tab[11] = Student("Konrad", "Lolek", 33, "lol@op.pl");

    }

void Navigation::generateLecturer()
    {
    tab3[0] = Lecturer("Pawel", "Pawlak", 50, "pp@o2.pl");
    tab3[1] = Lecturer("Wojtek", "Cieplak", 45, "wc@o2.pl");
    }

void Navigation::addStudent(std::string name, std::string surname, int age,
	std::string mail)
    {
    if (studentCounter < 100)
	{
	choiceStudent[studentCounter] = Student(name, surname, age, mail);
	}
    course1.enrolStudent(&choiceStudent[studentCounter]);
    studentCounter++;
    }

void Navigation::addLecturer(std::string name, std::string surname, int age,
	std::string mail)
    {
    if (lecturerCounter < 100)
	{
	choiceLecturer[lecturerCounter] = Lecturer(name, surname, age, mail);
	}
    course1.enrolLecturer(&choiceLecturer[lecturerCounter]);
    lecturerCounter++;
    }

void Navigation::addRoom(int number, int capacity, int building)
    {
    if (roomCounter < 10)
	{
	choiceRoom[roomCounter] = Room(number, capacity, building);
	}
    course1.assignRoom(&choiceRoom[roomCounter]);
    roomCounter++;
    }

void Navigation::addRandomCourse()
    {
    int i = rand() % (licznikCourse - 1) + 0;
    courses[courseCounter] = tab4[i];
    }

void Navigation::addRandomStudent()
    {
    if (licznik1 < course1.getPlaces())
	{
	int i = rand() % (licznikStudent - 1) + 0;
	course1.enrolStudent(&tab[i]);
	licznik1++;
	std::cout << "\tSuccess ! \n";
	}
    else
	{
	std::cout << "There's no more vacant places for a "
		<< course1.getTopic() << " course! " << std::endl;
	}
    }

void Navigation::addRandomLecturer()
    {
    if (licznik2 < course1.getLecturersAmount())
	{
	int i = rand() % (licznikLecturer - 1) + 0;
	course1.enrolLecturer(&tab3[i]);
	licznik2++;
	std::cout << "\tSuccess ! \n";
	}
    else
	{
	std::cout << "There's no more vacant places for a "
		<< course1.getTopic() << " course! "
		<< course1.getLecturersAmount() << " maximum." << std::endl;
	}
    }

void Navigation::addRandomRoom()
    {
    if (licznik3 < course1.getRoomsAmount())
	{
	int i = rand() % (licznikRoom - 1) + 0;
	course1.assignRoom(&tab2[i]);
	licznik3++;
	std::cout << "\tSuccess ! \n";
	}
    else
	{
	std::cout << "There's already " << course1.getRoomsAmount()
		<< " rooms assigned to this course! Enough " << std::endl;
	}
    }

void Navigation::showDetails()
    {
    course1.description();
    course1.getLecturer();
    course1.getStudents();
    }

void Navigation::showRandomCourseList()
    {
    for (int i = 0; i < licznikCourse; i++)
	{
	std::cout << "Course " << i + 1 << ":" << std::endl;
	tab4[i].description();
	}
    }
