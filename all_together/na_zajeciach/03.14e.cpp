//============================================================================
// Name        : 14e.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main()
{
    int powt(0);

    int tab[]={1,2,3,4,5,6,7};

    for(int i = 0; i < 7; i++)
    {
        for(int j = i+1; j < 7; j++)
        {
            if(tab[i]==tab[j])
                powt++;
        }
    }

    cout << sizeof(tab)/sizeof(tab[0])-powt << endl;

    return 0;
}
