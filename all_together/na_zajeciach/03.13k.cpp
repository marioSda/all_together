#include <iostream>
using namespace std;
//// /    Program wypisuj�cy r�nic� dw�ch kolejnych element�w tablicy ////

int main()
{

	const long long int max_size = 5;
	cout << "Podaj liczby: " << endl;
	int t[max_size] =
	{ };
	int temp;
	int roznica(0);

	for (int i = 0; i < max_size; ++i)
	{
		cin >> temp;

		if (temp != 0)
		{
			t[i] = temp;
		}
		else
			break;
	}

	for (int j = 0; j < max_size - 1; ++j)
	{
		roznica = t[j] - t[j + 1];
		cout << roznica << endl;
	}

	return 0;
}
