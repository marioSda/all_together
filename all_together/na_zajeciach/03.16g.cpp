#include <iostream>
using namespace std;

int main()
{
///// Program szukaj�cy elementu w tablicy. Zwraca indeks ////

	const long int max_size = 5;

	int t[max_size] = { 1, 2, 3, 4, 5 };
	int pozycja;
	cout << "jakiej liczby szukasz? ";
	cin >> pozycja;
	int *wskT = t;

	for (int i = 0; i < max_size; ++i)
	{
		if (*wskT == pozycja)
		{
			cout << "Pozycja szukanej liczby to: " << i + 1 << endl;
		}
		wskT++;
	}

	return 0;
}
