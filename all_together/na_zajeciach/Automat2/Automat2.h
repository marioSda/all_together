#ifndef AUTOMAT_H_
#define AUTOMAT_H_
#include <string>

const int MAXproducts = 20;

struct Product
{
	std::string brand;
	int price;
	int quantity;
};

class Automat
{
public:

	Automat();
	virtual ~Automat();

	void showWhatsInAutomat();
	void showSingleProduct(int index);
	void chooseProduct(int index);
	Product* getLastChosen();
	float putAmount(float amount);
	float cancelPurchase();

	void addProductToAutomat(std::string name, float price);
	void fillAutomatWithProducts();
	int getMoneyGien(){return moneyGiven;}

private:

	Product products[MAXproducts];
	int productCounter;
	int price;
	int moneyGiven;
	Product* chosenOne;
};

#endif
