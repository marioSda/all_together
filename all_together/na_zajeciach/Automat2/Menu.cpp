#include "Menu.h"
#include <iostream>
#include <stdlib.h>


Menu::Menu()
{

}

Menu::~Menu()
{

}

void Menu::ShowMenu()
{
	int choice;
	int coins;

	std::cout << "*** MENU ***\n" << std::endl;
	automat.showWhatsInAutomat();
	std::cout << "\nWhats your choice ? " << std::endl;
	std::cin >> choice;

	if(choice == 0)
	{
			std::cout << "Have a good day! " << std::endl;
		std::cin.clear();

	}else if (choice <= 20 || choice >= 1)
	{
		automat.chooseProduct(choice - 1);
		std::cout << "Chosen product: ";
		automat.showSingleProduct(choice - 1);
		std::cout << "Please pay: " << automat.getLastChosen()->price
				<< std::endl;

		while (true)
		{
			std::cout << "Coins: ";
			std::cin >> coins;
			std::cout << "The difference is: ";
			std::cout << automat.putAmount(coins) << std::endl;

			if(coins == 0)
			{
				cancel();
				std::cout << "\n=====================================\n" << std::endl;
				ShowMenu();

			}


			if (automat.getMoneyGien() >= automat.getLastChosen()->price)
			{
				std::cout << "Thank you! your product is ready to go! "
						<< std::endl;
				break;
			}
		}

	}

}

void Menu::cancel()
{
	automat.cancelPurchase();
}
