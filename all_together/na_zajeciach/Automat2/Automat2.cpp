#include "Automat2.h"
#include <string>
#include <iostream>
#include <cmath>

Automat::Automat()
{
	products[0] = {"snickers ", 180, 10};
	products[1] = {"mars ", 170, 10};
	products[2] = {"M&M ", 190, 10};
	products[3] = {"sparkling water ", 150, 10};
	products[4] = {"still water ", 150, 10};
	products[5] = {"orange juice ", 200, 10};
	products[6] = {"bread roll ", 250, 10};
	productCounter = 7;
	price = 0;
	moneyGiven = 0;
	chosenOne = 0;
}

Automat::~Automat()
{


}

void Automat::showSingleProduct(int index)
{
	if(index >=0 && index <= productCounter)
	{
		std::cout << index+1 << ". " << products[index].brand << " -- " << products[index].price << std::endl;
	}
}

void Automat::showWhatsInAutomat()
{
	for(int i = 0; i < productCounter; i++)
	{
		showSingleProduct(i);
	}
}

void Automat::chooseProduct(int index)
{
	if(index >=0 && index <= productCounter)
	{
		chosenOne = &products[index];
		price = chosenOne->price;
		chosenOne = &products[index];
	}
}

Product* Automat::getLastChosen()
{
	return chosenOne;
}

float Automat::putAmount(float amount)
{
	float excess;

	if(price!=0)
	{
		if(chosenOne->price-amount>=0)

		{
			price -= amount;
			moneyGiven	+= amount;

		if(price == 0)
			return 0;
		else if(price < 0)
		{
			excess = price;
			price = 0;
			return std::abs(excess);
		}
		return -price;
		}
	}
	return 1234;
}

float Automat::cancelPurchase()
{
	float excess;

	if(price != 0 && price < chosenOne->price)
	{
		price = 0;
		return moneyGiven;
	}

	chosenOne = 0;
	price = 0;
	return 0;
}

void Automat::addProductToAutomat(std::string name, float price)
{
	products[productCounter] = {name, price};
	productCounter++;
}

void Automat::fillAutomatWithProducts()
{
	for(int i = 0; i < MAXproducts; i++)
	{
		products[i] = {"random "+(i+1), 1+i*0.10};
	}
}
