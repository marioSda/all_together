#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

	vector<int> myVector;
	for(int i = 1; i < 10; i++)
	{
		myVector.push_back(i*10);
	}

	ostream_iterator<int> out_it(cout, ", ");
	copy(myVector.begin(), myVector.end(), out_it);



	return 0;
}
