#include <iostream>
using namespace std;

int main() {

	int* myInt_;
	float* myFloats_;
	double* myDoubles_[2];
	bool** myBools_;

	myFloats_ = new float[3];
	myFloats_[0] = 3.0f;
	myFloats_[1] = 3.1f;
	myFloats_[2] = 3.2;

	myDoubles_[0] = new double[2];
	myDoubles_[1] = new double[2];
	myDoubles_[0][0] = 0.006;
	myDoubles_[0][1] = 0.016;
	myDoubles_[1][0] = 0.106;
	myDoubles_[1][1] = 0.116;

	myBools_ = new bool*[2];
	myBools_[0] = new bool(true);
	myBools_[1] = new bool(false);

	cout << "myFloats: " << myFloats_<<endl;
	cout << "myFloats: " << *myFloats_<<endl;
	cout << "myFloats: " << myFloats_[0]<<endl;

	cout << "myDoubles: " << myDoubles_<<endl;
	cout << "*myDoubles: " << *myDoubles_<<endl;
	cout << "**myDoubles: " << **myDoubles_<<endl;
	cout << "myDoubles[0]: " << myDoubles_[0]<<endl;
	cout << "*myDoubles[0]: " << *myDoubles_[0]<<endl;
	cout << "**myDoubles[0]: " << **myDoubles_<<endl;





	return 0;
}
