#include <list>
#include <iostream>
using namespace std;

int main()
{

	int tab[] =
	{ 1, 3, 52, 2, 4, 2, 6, 7 };
	int tab2[] =
	{ 8, 7, 6, 5, 4, 3, 2, 1 };
	int tab3[] =
	{ 1,2,3,4,5,6,7,8};

	list<int> first(tab, tab + 8);
	list<int> second(tab2, tab2 + 8);


	first.merge(second);

	for (list<int>::iterator i = first.begin(); i != first.end(); i++)
	{
		cout << *i << " ";
	}
	cout << endl;

	first.sort();
	second.sort();

	first.merge(second);
	cout << "po sortowaniu: " << endl;
	for (list<int>::iterator i = first.begin(); i != first.end(); i++)
	{
		cout << *i << " ";
	}

//////////////////////////////////////////////////////////////////////////////////////
	cout << "\nWstawianie " << endl;
	list<int> third(5, 500);
	list<int> fourth(10);


	list<int>::iterator it = fourth.begin();
	it++;
	it++;


	fourth.splice(it, third);

	for (list<int>::iterator i = fourth.begin(); i != fourth.end(); i++)
	{
		cout << *i << " ";
	}



	return 0;
}
