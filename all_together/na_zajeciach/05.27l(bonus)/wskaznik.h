#ifndef WSK_H_
#define WSK_H_

int* ptr = 0;
int (*fun_ptr)(void*,int) = 0;

int* init_ptr(int siz)
{
	int* ptr = new int[siz];
	for (int i = 0; i < siz; i++)
	{
		ptr[i] = i + 1;
	}
	return ptr;
}

void print_ptr(int* ptr, int siz)
{
	for (int i = 0; i < siz; i++)
	{
		std::cout << "[" << *ptr++ << "]," << " ";
	}
}

int* reinit_ptr(int* ptr, int siz)
{
	delete[] ptr;

	ptr = new int[siz];
		for (int i = 0; i < siz; i++)
		{
			ptr[i] = siz-i;
		}
		return ptr;
}

void deinit_ptr(int* ptr)
{
	delete[] ptr;
}

int sum(void* pt, int siz)
{
	int suma = 0;

	int* ptr = reinterpret_cast<int*>(pt);

	for(int i = 0; i < siz; i++)
	{
		suma += ptr[i];
	}

	return suma;
}

int iloczyn(void* pt, int siz)
{
	int iloczyn = 1;

	int* ptr = reinterpret_cast<int*>(pt);

	for(int i = 0; i < siz; i++)
	{
		iloczyn *= ptr[i];
	}
	return iloczyn;
}


#endif
