#include <iostream>
#include "wskaznik.h"
using namespace std;

int main()
{
	int size = 10;
	ptr = init_ptr(size);
	cout << "----------------------------------------\n";

	print_ptr(ptr, size);
	cout << "\n----------------------------------------\n";

	ptr = reinit_ptr(ptr, size);

	print_ptr(ptr, size);
	cout << "\n----------------------------------------";

	fun_ptr = sum;
	cout << "\nSuma = " << fun_ptr((void*) ptr, size);
	cout << "\n----------------------------------------";
	fun_ptr = iloczyn;
	cout << "\nIlocz = " << fun_ptr((void*) ptr, size) << endl;
	deinit_ptr(ptr);

	return 0;
}
