#include <iostream>
using namespace std;

void passByValue(int x);
void passByReference(int *x);

int main()
{
//	int random = 5;
//
//	cout << &random << endl;
//
//	int *fishPointer = &random;
//
//	cout << fishPointer << endl;

	int betty = 13;
	int sandy = 13;
	passByValue(betty);
	passByReference(&sandy);
	cout << betty << endl;
	cout << sandy << endl;
	return 0;
}

void passByValue(int x)
{
	x = 99;
}

void passByReference(int *x)
{
	*x = 66;
}
