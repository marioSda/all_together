#include <iostream>
using namespace std;

int main()
{
	int rok;
	cout << "Wprowadz rok ";
	cin >> rok;

	if ((rok % 4) == 0 && (rok % 100) != 0 && rok != 0)
	{
		cout << "Rok jest przestÍpny! " << endl;
	}
	else if ((rok % 400) == 0 && rok != 0)
		cout << "Rok jest przestÍpny! " << endl;
	else
		cout << "Rok nie jest przestÍpny! " << endl;

	return 0;
}
