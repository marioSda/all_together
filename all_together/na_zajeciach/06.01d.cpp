#include <iostream>
#include <vector>

using namespace std;

int main()
{

	int tab[] =
	{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	vector<int> first(tab, tab + 9);
	int tab2[] =
	{ 243, 424, 23, 23, 553 };
	vector<int> second(tab2, tab2 + 5);

	vector<int>::iterator it = first.begin() + 3;
	vector<int>::iterator begin = second.begin();
	vector<int>::iterator end = second.end();

	copy(begin, end, it);

	for (int i = 0; i < 9; i++)
	{
		cout << first[i] << " ";
	}

	for (int i = 0; i < 9; i++)
	{
		first[i] = i + 1;
	}

	cout << endl;

	copy(begin, end, inserter(first, it));

	for (vector<int>::iterator it = first.begin(); it != first.end(); it++)
	{
		cout << *it << " ";
	}

	return 0;
}
