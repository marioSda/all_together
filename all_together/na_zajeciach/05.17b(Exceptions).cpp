#include <iostream>
using namespace std;

int* tab(int size)
{

	int* wsk = new int[size];
	for (int i = 0; i < size; i++)
	{
		wsk[i] = i + 1;
	}
	return wsk;

	return 0;
}

int* tabNoThrow(int size)
{

	int* wsk = new (nothrow) int[size];
	for (int i = 0; i < size; i++)
	{
		wsk[i] = i + 1;
	}
	return wsk;

	return 0;
}

int main()
{

	int size = 100000000000;
	int *firstArray;
	int *secondArray;

	try
	{
		firstArray = tab(size);
//		secondArray = tabNoThrow(size);
		if(!secondArray)
		{
			bad_alloc e;
			throw e;
		}

	} catch (bad_alloc& e)
	{
		cout << "Wyjatek przy alokacji!" << endl;
		cout << "Bad alloc " << e.what() << endl;
	}catch (...)
	{
		cout << "Unrecognized exception " << endl;
	}

	return 0;
}
