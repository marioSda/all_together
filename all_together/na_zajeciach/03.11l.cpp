#include <iostream>
using namespace std;

int main()
{

	int kwota;
	int banknotPiecset(0);
	int banknotDwiescie(0);
	int banknotSto(0);
	int banknotPiecdziesiat(0);
	int banknotDwadziescia(0);
	int banknotDziesiec(0);
	int reszta(0);

	cout << "Podaj kwote: ";
	cin >> kwota;

	while (kwota >= 500)
	{
		kwota -= 500;
		banknotPiecset++;
	}
	while (kwota >= 200)
	{
		kwota -= 200;
		banknotDwiescie++;
	}
	while (kwota >= 100)
	{
		kwota -= 100;
		banknotSto++;
	}
	while (kwota >= 50)
	{
		kwota -= 50;
		banknotPiecdziesiat++;
	}
	while (kwota >= 20)
	{
		kwota -= 20;
		banknotDwadziescia++;
	}
	while (kwota >= 10)
	{
		kwota -= 10;
		banknotDziesiec++;
	}


	cout<< "Do wyplacenia kwoty potrzeba:\n " << banknotPiecset << " banknotow 500,\n "
											  << banknotDwiescie << " banknotow 200, \n "
											  << banknotSto << " banknotow 100, \n "
											  << banknotPiecdziesiat << " banknotow 50, \n "
											  << banknotDwadziescia << " banknotow 20, \n "
											  << banknotDziesiec << " banknotow 10, \n "
											  << kwota << " reszty" << endl;
	return 0;
}
