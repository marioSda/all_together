/*
 * Z.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "Z.h"
#include <iostream>

Z::Z()
{
	std::cout << "Constructor from class Z " << std::endl;

}

Z::~Z()
{
	std::cout << "Destructor from class Z " << std::endl;
}

