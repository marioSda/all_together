#include <iostream>
#include "A.h"
#include "B.h"
#include "C.h"
#include "D.h"

using namespace std;

void f(A *someA)
{
	cout << someA->number << endl;
	someA->number = 5;
	cout << someA->number << endl;
}

int main() {

//	cout << "Starting the program! " << endl;
//
//	A a;
//	a.number=2;
//	f(&a);
//	cout << a.number << endl;
//	cout << a;
//	operator<<(cout, a);
//	cout << "Exiting the program! " << endl;

	//A tab[3] = {A(),A(),A()};
//	cout << tab[0].number << endl;

	//B b;
	cout << "**************************" << endl;
	//D d;
	cout << "**************************" << endl;
	A *a = new D;	/// virutal musi byc w dzidziczonych klasach przy public oraz destruktor w A powinien byc wirtualny

	delete a;


	return 0;
}
