/*
 * B.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "B.h"
#include <iostream>

B::B()
{
	std::cout << "Constructor from class B " << std::endl;
}

B::~B()
{
	std::cout << "Destructor from class B " << std::endl;

}
