#include "A.h"
#include <iostream>

A::A()
{
	std::cout << "Constructing A " << std::endl;
	number = 0;
}

A::~A()
{
	std::cout << "Destroying A " << std::endl;
}

std::ostream& operator<<(std::ostream& stream, const A& a)
{
	stream << "Content of a:\n\t number: " << a.number << "\n";
	return stream;
}
