/*
 * D.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "D.h"
#include <iostream>

D::D()
{
	std::cout << "Constructor from class D " << std::endl;

}

D::~D()
{
	std::cout << "Destructor from class D " << std::endl;
}

