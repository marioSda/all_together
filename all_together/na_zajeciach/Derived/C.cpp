/*
 * C.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "C.h"
#include <iostream>

C::C()
{
	std::cout << "Constructor from class C " << std::endl;

}

C::~C()
{
	std::cout << "Destructor from class C " << std::endl;}

