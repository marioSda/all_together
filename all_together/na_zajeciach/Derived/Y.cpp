/*
 * Y.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "Y.h"
#include <iostream>

Y::Y()
{
	std::cout << "Constructor from class Y " << std::endl;

}

Y::~Y()
{
	std::cout << "Destructor from class Y " << std::endl;
}

