#ifndef A_H_
#define A_H_
#include <iostream>
class B;

class A
{
public:
	friend B;
	A();
	virtual ~A();
	int number;
};
	std::ostream& operator<<(std::ostream& stream, const A& a);



#endif
