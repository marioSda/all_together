#include <iostream>
using namespace std;

int main()
{
	int a, b, operacja;
	bool flaga = true;

	while (flaga)
	{
		cout << "*******************************************" << endl;
		cout << "Podaj dwie liczby \n"; cin >> a >> b;
		cout << "Jak� operacj� chcesz przeprowadzic? \n";
		cout << "1. dodawanie \n";
		cout << "2. odejmowanie \n";
		cout << "3. mnozenie \n";
		cout << "4. dzielenie \n";

		cin >> operacja;

		if (operacja == 1)
			cout << a << " + " << b << " = " << a + b << endl;
		else if (operacja == 2)
			cout << a << " - " << b << " = " << a - b << endl;
		else if (operacja == 3)
			cout << a << " * " << b << " = " << a * b << endl;
		else if (operacja == 4)
			cout << a << " / " << b << " = " << a / b << endl;
		else
		{
			cout << "Prosze podac liczbe od 1 do 4 " << endl;
			flaga = false;
		}
	}
	return 0;
}
