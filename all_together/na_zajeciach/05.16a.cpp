#include <iostream>
using namespace std;

void last()
{
	cout << "Last start " << endl;
	cout << "throwing double exception " << endl;
	throw -1.0;


	cout << "Last end " << endl;
}

void third()
{
	cout << "Third function " << endl;

	last();

	cout << "Third end " << endl;
}

void second()
{
	cout << "Second function " << endl;

	try
	{
		third();
	} catch (double)
	{
		cerr << "in second double exception " << endl;
	}
	cout << "Second end " << endl;
}

void first()
{
	cout << "First function " << endl;

	try
	{
		second();
	} catch (int)
	{
		cerr << "in first int exception " << endl;
	} catch (double)
	{
		cerr << "in first double exception " << endl;
	}
	cout << "First end " << endl;
}

int main()
{
	cout << "main start " << endl;

	try
	{
		int x = 2;
		int y = 0;
		double c = 0.2;
		if(y==0)
			throw "cokolwiek";
		int d = x/y;

	} catch (exception& e)
	{
		cout << "in main int exception catched " << endl;
	}

	cout << "main end " << endl;

	return 0;
}
