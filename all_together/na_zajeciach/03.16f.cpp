#include <iostream>
#include <cmath>
using namespace std;

//////  Program wypisuj�cy 3 pot�g� liczb w tablicy ///////

int main()
{
	const long int max_size = 5;
	cout << "Podaj liczby: " << endl;
	int t[max_size] = { };
	int temp;
	int *wskT = t;

	for (int i = 0; i < max_size; ++i)
	{
		cin >> temp;
		*wskT = temp;
		wskT++;
	}
	wskT=t;
	for (int j = 0; j < max_size; ++j, wskT++)
	{

		cout << pow(*wskT,3) << " ";
	}

	return 0;
}
