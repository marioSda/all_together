#include <iostream>
using namespace std;


int solution(int* A, int size_of_A)
{

	int length1 = 0;
	int length2 = 0;

	int index;
	for(int i = 0, j = i+1; j < size_of_A; i++, j++)
	{
		if(A[i]<A[j])
		{
			length1++;
		}else
		{
			if(length2 < length1)
			{
				length2 = length1;
				index = i - length1;
			}

			length1 = 0;
		}
	}

	return index;
}


int main() {

	const int size = 16;
	int tab[size] = {2,2,2,2,1,2,3,-1,2,3,4,1,3,4,5,6};

	cout << solution(tab,size) << endl;







	return 0;
}
