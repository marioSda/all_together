#include <iostream>
using namespace std;
///////////////////////// Pobieranie liczb i wypisywanie ich na ekran w odwrotnej kolejnosci ///////////////
int main()
{

	const long long int max_size = 10;
	cout << "Podaj liczby: " << endl;
	int t[max_size] ={};
	int temp;

	for (int i = 0; i < max_size; ++i)
	{
		cin >> temp;

		if (temp != 0)
		{
			t[i] = temp;
		}
		else
			break;
	}

	for (int j = max_size-1; j >= 0; --j)
	{
		cout << t[j] << " ";
	}

	return 0;
}
