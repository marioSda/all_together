#include <iostream>
#include <math.h>
#include <exception>

using namespace std;

class edomException: public exception
{
public:
	virtual const char* what() const throw ()
	{
		return "EDOM exception";
	}
};

class erangeException: public exception
{
public:
	virtual const char* what() const throw ()
	{
		return "ERANGE exception";
	}
};

void displayException(exception& e)
{
	cout << "exception: " << e.what() << endl;
}

double myPow(double x, double y)
{
	double power = pow(x, y);

	if (errno == EDOM)
	{
		edomException e;
		throw e;
	}
	if (errno == ERANGE)
	{
		erangeException e;
		throw e;
	}

	return power;

}

double myLog(double x)
{
	double logg = log(x);

	if (errno == EDOM)
	{
		edomException e;
		throw e;
	}
	if (errno == ERANGE)
	{
		erangeException e;
		throw e;
	}
	return logg;
}

int main()
{

//	int a = pow(10000, 10000);	// domain error / range_error
//	int b = log(0.5);
//	if(errno == EDOM)
//	{
//		cout << "EDOM" << endl;
//	}
//	if(errno == ERANGE)
//	{
//		cout << "ERANGE" << endl;	// wyjscie poza zakres
//	}
//
//	cout << "a: " << a << endl;

	try
	{
		double x = 50000000;
		double y = 100000;
		double z = -10;
		double pow = myPow(x, y);
		double log = myLog(z);
	} catch (exception& e)
	{
		cout << "Exception " << e.what() << endl;
	}

	return 0;
}
