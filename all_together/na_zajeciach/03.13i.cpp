#include <iostream>
#include <cmath>
using namespace std;
///// Program wypisuj�cy liczby kt�rych suma cyfr jest parzysta z tablicy ///////
int main()
{
	const long int max_size = 5;
	cout << "Podaj liczby: " << endl;
	int t[max_size] ={};
	int temp;
	int cyfra = 0;
	int suma = 0;

	for (int i = 0; i < max_size; ++i)
	{
		cin >> temp;

		if (temp != 0)
			t[i] = temp;
		else
			break;
	}

	for (int j = 0; j <= max_size; ++j)
	{
		suma = 0;
		temp = t[j];
		while(temp > 0)
		{
			cyfra = temp % 10;
			temp /= 10;
			suma += cyfra;
		}

		if(suma % 2 == 0)
			cout << t[j] << " ";
	}

	return 0;
}
