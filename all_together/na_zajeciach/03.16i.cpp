#include <iostream>
using namespace std;
/// kalkulator - funkcje ////
double dodawanie(double *a, double *b);
double odejmowanie(double *a, double *b);
double mnozenie(double *a, double *b);
double dzielenie(double *a, double *b);

int main() {

	double a, b, c;
	cout <<"Podaj dwie liczby: ";
	cin >> a >> b ;
	cout <<"Jakie dzialanie chcesz przeprowadzic? \n"
			"1. dodawanie\n"
			"2. odejmowanie\n"
			"3. mnozenie\n"
			"4. dzielenie\n" << endl;
	cin >> c;
	if(c==1)cout<<dodawanie(&a,&b)<<endl;
	if(c==2)cout<<odejmowanie(&a,&b)<<endl;
	if(c==3)cout<<mnozenie(&a,&b)<<endl;
	if(c==4)cout<<dzielenie(&a,&b)<<endl;
	if(c>4||c<1)cout<<"Wprowadz z przedzialu 1-4 \n";

	return 0;
}

double dodawanie(double *a, double *b){return *a+*b;}
double odejmowanie(double *a, double *b){return *a-*b;}
double mnozenie(double *a, double *b){return *a**b;}
double dzielenie(double *a, double *b)
{
	if(*b!=0)return *a/(*b);
	else return 0;
}
