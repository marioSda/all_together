#include <iostream>
using namespace std;

int solution(int *A, int size_of_A)
{
	int min = A[0];
	for(int i = 1; i < size_of_A; i++)
	{
		if(A[i]<min)
			min = A[i];
	}
	return min;
}

int main() {

	const int N = 10;
	int A[N] = {1,2,3,4,5,6,7,8,9,10};
	int A2[N] = {-2, 147, 483, 3, 20, 434, 3, 3, 45, 33};
	int A3[N] = {0.1, 147, 100000000, 3, 20, 434, 0, 3, 45, 33};
	int A4[N] = {0.1, 147, -100000000, 3, 20, 434, 0, 3, 45, 33};
	int A5[N] = {-0.1, 147, 483, 3, 20, 434, 3, 3, 45, 33};

	cout << solution(A, N) << endl;
	cout << solution(A2, N) << endl;
	cout << solution(A3, N) << endl;
	cout << solution(A4, N) << endl;
	cout << solution(A5, N) << endl;


	return 0;
}
