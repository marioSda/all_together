#include <iostream>
using namespace std;

int main()
{
	int a, b;

	cout << "Podaj zakres: ";
	cin >> a >> b;

	if (a % 2 != 0)
		a += 1;

	for (int i = a; i <= b; i += 2)
	{
		cout << i << endl;
	}

	return 0;
}
