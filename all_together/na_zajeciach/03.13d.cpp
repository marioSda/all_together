#include <iostream>
using namespace std;
//////////////// Program wyswietlajacy min i max z tablicy ////////////////
int main()
{

	const long int max_size = 5;
	cout << "Podaj liczby: " << endl;
	int t[max_size] ={};
	int temp;
	int max = 1;
	int min = max+1;

	for (int i = 0; i < max_size; ++i)
	{
		cin >> temp;

		if (temp != 0)
		{
			t[i] = temp;
			if (t[i] > max)
				max = t[i];

			if (t[i] < min)
				min = t[i];
		}
		else
			break;
	}

	cout << "Max: " << max << endl;
	cout << "Min: " << min << endl;

	return 0;
}
