#include <iostream>
#include <vector>

using namespace std;

int main()
{
	vector<int> myVector(5);

	int i = 0;

	vector<int>::reverse_iterator rit = myVector.rbegin();

	for(; rit != myVector.rend(); rit++)
		*rit = ++i;

	cout << "myVector contains: ";

	for(vector<int>::iterator it = myVector.begin(); it != myVector.end(); it++)
		cout << ' ' << *it;
	cout << '\n';


	vector<int> first(10);

	for(int i = 0; i < 10; i++)
		first[i] = i;


	vector<int>::reverse_iterator j = first.rbegin();
	advance(j,3);
	vector<int>::reverse_iterator it;
	for(it = first.rbegin(); it != j; it++)
		*it*=*it;

	for(int i = 0; i < 10; i++)
		cout << first[i] << " ";

	vector<int> second(10);
	it = second.rbegin();


	copy(first.begin(), first.end(), it);

	cout << endl;
	for(int i = 0; i < 10; i++)
		cout << second[i] << " ";



	return 0;
}
