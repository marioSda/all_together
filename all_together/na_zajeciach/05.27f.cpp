#include <iostream>
#include <vector>
using namespace std;

int main()
{

	vector<int> myVector;
	int temp=1;

	cout << "Put few random numbers (0 makes it stop):" << endl;
	while (true)
	{
		cin >> temp;
		if (!temp)
			break;
		else
			myVector.push_back(temp);

	}

	for(int i = 0; i < myVector.size(); i++)
	{
		cout << myVector.at(i) << " ";
	}

	cout << endl;
	/// Another way to do it /////////////////////////////////////////////////

	int temp2=1;

	cout << "How long vector? " << endl;
	cin >> temp2;
	vector<int> myVector2;
	myVector2.resize(temp2);

	cout << "Put " << temp2 << " the numbers: " << endl;


	for(int i = 0; i < myVector2.size(); i++)
	{
		cin >> temp2;
		myVector2[i] = temp2;
	}

	for(int i = 0; i < myVector2.size(); i++)
		{
			cout << myVector2[i] << " ";
		}

	cout << endl;

	return 0;
}
