#include <iostream>
using namespace std;
/// iloczyn wektorow /////
void iloczyn(int *p1, int *p2, int *p3, int n);

int main() {

	int tab1[3]={2,3,4};
	int tab2[3]={5,6,7};
	int tab3[3]={};

	int *pTab1=tab1;
	int *pTab2=tab2;
	int *pTab3=tab3;

	iloczyn(pTab1, pTab2, pTab3, 3);

	for(int i = 0; i < 3; i++, pTab3++)
		cout << *pTab3 << endl;

	return 0;
}

void iloczyn(int *p1, int *p2, int *p3, int n)
{
	for(int i = 0; i < 3; i++,p3++,p2++,p1++)
		*p3=*p1*(*p2);
}
