#include <iostream>
#include <cstdlib>
#include <time.h>
#include <cmath>
#include <iomanip>
/// sortowanie przez wstawianie z uzyciem wskaznikow ///
using namespace std;

const int N = 20;

int main()
{
    int temp, i, j;
    int tab[N]= {};

    srand((int)time(NULL));
    int *wsk = tab;

    for(i=0; i< N; i++, wsk++)*wsk = rand()% 100;
    wsk=tab;
    for(i=0; i<N; i++, wsk++)cout<<*wsk<<" ";
    wsk=tab;
    cout << endl;

    for(j=N-2; j>=0; j--)
    {
        temp = tab[j];
        i = j + 1;
        while(i<N && temp>tab[i])
        {
            tab[i-1] = tab[i];
            i++;
        }
        tab[i-1] = temp;
    }

    for(i=0; i<N; i++, wsk++)cout<<*wsk<<" ";

    return 0;
}
