#include <iostream>
#include <list>

using namespace std;

int main() {

	int tab[] = {1,2,3,4,5,6,7,8};
	list<int>first(tab,tab+8);
	list<int>second(10,1);

	list<int>::iterator it1 = first.begin();
	advance(it1,3);


	list<int>::iterator input1 = second.begin();
	advance(input1,3);
	list<int>::iterator input2 = second.end();
	advance(input2,8);

	first.splice(it1,second,input1,input2);

	for(list<int>::iterator i = first.begin(); i != first.end(); i++)
	{
		cout << *i << " ";
	}


	return 0;
}
