#include <iostream>
#include <iomanip>
#include <time.h>
#include "gtest.h"

using namespace std;

const int size = 10;
int tab[size] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };

void display(int tab[]);
void reverse(int tab[]);
void bubble_sort(int tab[]);
void insertion_sort(int tab[]);
void quick_sort(int first, int last);

int main()
{

	clock_t tStart = clock();

	insertion_sort(tab);
		display(tab);
		reverse(tab);
	bubble_sort(tab);
		display(tab);
		reverse(tab);
	quick_sort(0, 9);
		display(tab);
		reverse(tab);

		cout << "Time taken: " << fixed << setprecision(3) << (1.0*clock() - tStart) / CLOCKS_PER_SEC << "\n";

	return 0;
}

void display(int tab[])
{
	for (int i = 0; i < size; i++)
	{
		cout << tab[i] << " ";
	}
	cout << endl;
}

void bubble_sort(int tab[])
{
	int temp;

	for (int i = 0; i < size; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (tab[i] > tab[j])
			{
				temp = tab[j];
				tab[j] = tab[i];
				tab[i] = temp;
			}
		}
	}
	cout << endl;
}

void reverse(int tab[])
{
	int temp;

	for (int i = 0; i < size; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (tab[i] < tab[j])
			{
				temp = tab[j];
				tab[j] = tab[i];
				tab[i] = temp;
			}
		}
	}
	cout << endl;
}

void merge_sort(int first, int last)
{
	int temp[size];
	int middle = (first + last + 1)/2;		// indeks srodkowego elementu

	if(middle - first > 1) merge_sort(first,middle-1);
	if(last - middle > 0) merge_sort(middle, last);

	int i1 = first;
	int i2 = middle;

	for(int i = first; i <= last; i++)
	{

	}


}

void insertion_sort(int tab[])
{
	int temp, j;

	for(int i = size - 2; i >= 0; i--)
	{
		temp = tab[i];
		j = i + 1;
		while((j < size) && (temp > tab[j]))
		{
			tab[j - 1] = tab[j];
			j++;
		}
		tab[j-1] = temp;
	}
	cout << endl;
}

void quick_sort(int first, int last)
{

	int i, j , piwot;

	i = (first + last)/2;
	piwot = tab[i];
	tab[i] = tab[last];

	for(j = i = first; i < last; i++)
	{
		if(tab[i] < piwot)
		{
			swap(tab[i], tab[j]);
			j++;
		}
	}

	tab[last] = tab[j];
	tab[j] = piwot;
	if(first < j - 1)quick_sort(first, j-1);
	if(j + 1 < last)quick_sort(j + 1, last);
}
