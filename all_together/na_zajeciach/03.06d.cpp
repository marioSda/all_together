#include <iostream>
using namespace std;

int main()
{
	for (int i = 2; i <= 100; i++)
	{
		if (i % 2 == 0)
		{
			cout << i << endl;
		}
	}

	//////////////////////////////////////////////////

	int a, b;

	cout << "Podaj zakres: "; cin >> a >> b;

	for (int i = a; i <= b; i++)
	{
		if (i % 2 == 0)
		{
			cout << i << endl;
		}
	}

	////////////////////////////////////////////////

	for(int i = 0; i <= 100; i++)
		{
			if(i%2!=0)
				continue;
			cout << i << endl;
		}

	////////////////////////////////////////////////

	return 0;
}
