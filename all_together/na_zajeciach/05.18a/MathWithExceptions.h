#ifndef MATHWITHEXCEPTIONS_H_
#define MATHWITHEXCEPTIONS_H_

#include <iostream>
#include <math.h>
#include <exception>


using namespace std;

namespace math
{

class MathWithExceptions
{


public:

	class edomException: public exception
	{
	public:
		edomException(double number);
		edomException();
		virtual const char* what() const throw (){return "EDOM exception";}
	};

	class erangeException: public exception
	{
	public:
		erangeException(double number);
		erangeException();
		virtual const char* what() const throw (){return "ERANGE exception";}
	};

	void displayException(exception& e){cout << "exception: " << e.what() << endl;}

	MathWithExceptions();
	virtual ~MathWithExceptions();

	static double log(double number);
	static int pow(int number, int power);
	static double sin(double number);
	static double cos(double number);
	static double tan(double number);


};
}
#endif

