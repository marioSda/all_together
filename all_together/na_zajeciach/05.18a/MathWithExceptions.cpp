#include "MathWithExceptions.h"

using namespace math;

MathWithExceptions::MathWithExceptions()
{

}

MathWithExceptions::~MathWithExceptions()
{

}

double MathWithExceptions::log(double number)
{
	double result;

	result = ::log(number);

	if (errno == EDOM)
	{
		edomException e(number);
		throw e;
	}
	if (errno == ERANGE)
	{
		erangeException e;
		throw e;
	}

	return result;
}

int MathWithExceptions::pow(int number, int power)
{
	int result;

	result = ::pow(number, power);

	if (errno == EDOM)
	{
		edomException e;
		throw e;
	}
	if (errno == ERANGE)
	{
		erangeException e;
		throw e;
	}

	return result;
}

double math::MathWithExceptions::sin(double number)
{
	double result;
	result = ::sin(number);

	if (errno == EDOM)
	{
		edomException e;
		throw e;
	}
	if (errno == ERANGE)
	{
		erangeException e;
		throw e;
	}

	return result;

}

double math::MathWithExceptions::cos(double number)
{
	double result;
	result = ::cos(number);

	if (errno == EDOM)
	{
		edomException e;
		throw e;
	}
	if (errno == ERANGE)
	{
		erangeException e;
		throw e;
	}
	return result;
}

double math::MathWithExceptions::tan(double number)
{
	double result;
	result = ::tan(number);

	if (errno == EDOM)
	{
		edomException e;
		throw e;
	}
	if (errno == ERANGE)
	{
		erangeException e(number);
		throw e;
	}

	return result;
}

math::MathWithExceptions::edomException::edomException()
{
	cout << "*****Bledna wartosc wprowadzona *******" <<  flush;
}


math::MathWithExceptions::edomException::edomException(double number)
{
	cout << "*****Bledna wartosc wprowadzona, number = " << number << " ****** " <<  flush;
}

math::MathWithExceptions::erangeException::erangeException()
{
	cout << "******Wartosc poza zakresem! ******" << flush;
}

math::MathWithExceptions::erangeException::erangeException(double number)
{
	cout << "******Bledna wartosc: " << number << " poza zakresem!  " << flush;

}



