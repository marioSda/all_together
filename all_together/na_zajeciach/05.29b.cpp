#include <iostream>
#include <list>
using namespace std;

int main()
{

	list<int> first;
	list<int> second(4, 100);
	list<int> third(second.begin(), second.end());
	list<int> fourth(third);

	int myInts[] =
	{ 16, 2, 77, 29 };
	list<int> fifth(myInts, myInts + sizeof(myInts) / sizeof(int));

	cout << "fifth: " << endl;
	for (list<int>::iterator it = fifth.begin(); it != fifth.end(); it++)
	{
		cout << *it << " ";
	}

	cout << endl;
	cout << "second: " << endl;
	second.push_front(2);
	second.push_back(2);

	for (list<int>::iterator it = second.begin(); it != second.end(); it++)
	{
		cout << *it << " ";
	}

	cout << endl;

	second.pop_back();

	for (list<int>::iterator it = second.begin(); it != second.end(); it++)
	{
		cout << *it << " ";
	}

	cout << endl;

	list<int> random(8, 5);

	random.resize(10, 5);

	for (list<int>::iterator it = random.begin(); it != random.end(); it++)
	{
		cout << *it << " ";
	}

	cout << endl;

	list<int>::iterator it = random.begin();
	random.insert(it, 300);
	it = random.begin();
	it++;
	;
	random.insert(it, 2, 300);
	random.insert(it, 2, 400);

	for (list<int>::iterator it = random.begin(); it != random.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;
	it = random.begin();
	it++;
	it++;
	it++;
	it++;
	list<int>::iterator sec;
	sec = random.end();
	sec--;
	sec--;

	random.erase(it, sec);

	for (list<int>::iterator it = random.begin(); it != random.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	list<int> newone(5, 5);
	it = newone.begin();
	sec = newone.end();
	sec--;
	newone.erase(it, sec);
	for (list<int>::iterator it = newone.begin(); it != newone.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	it = newone.begin();
	newone.erase(it);
	for (list<int>::iterator it = newone.begin(); it != newone.end(); it++)
	{
		cout << "pusta? " << *it << " ";
	}
	cout << *it << endl;

	return 0;
}
