#include <iostream>
using namespace std;

int main()
{
	int v1, v2, liczba;

	cout << "Podaj wspolrzedne wektora: ";
	cin >> v1 >> v2;
	cout << "Podaj liczbe przez jaka chcesz pomnozyc wektor: ";
	cin >> liczba;

	int vector1[2] = {v1, v2};

	cout << "Wspolrzedne wektora 1: ";
	for (int i = 0; i < 2; i++)
		cout << vector1[i] << " ";

	cout << endl;

	int suma[2] = {};

	for (int i = 0; i < 2; i++)
		suma[i] = vector1[i] * liczba;

	cout << "Wspolrzedne wektora bedacego iloczynem "<< liczba << ", wynosi: ";

	for (int j = 0; j < 2; j++)
		cout << suma[j] << " ";

	return 0;
}
