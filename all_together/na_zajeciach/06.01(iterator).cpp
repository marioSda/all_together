#include <iostream>
#include <iterator>
using namespace std;

int main() {

	double value1, value2;

	cout << "Please, insert two values: ";

	istream_iterator<double> eos;
	istream_iterator<double> iit(cin);

	if(iit!=eos) value1 =* iit;

	++iit;
	if(iit!=eos) value2 = *iit;

	cout << value1 << "*" << value2 << "=" << (value1*value2) << '\n';

	return 0;
}
