#include "myArray.h"

myArray::myArray()
{
	size = 0;
	tab = new int[size];
	tab2 = 0;
	last_element = tab;
}

myArray::~myArray()
{
	if (size > 0)
		delete[] tab;
}

int* myArray::at(int x)
{
	if (x < size)
	{
		return &tab[x];
	}
	else
		return 0;
}

void myArray::resize(int new_size)
{
	tab2 = tab;

	tab = new int[new_size];
	for (int i = 0; i < size; i++)
	{
		tab[i] = tab2[i];
	}
	delete[] tab2;
	size = new_size;
}

void myArray::append(int element_to_add)
{
	tab[size] = element_to_add;
	last_element = &tab[size];
	size += 1;

}

void myArray::showUp()
{
	for (int i = 0; i < size; i++)
	{
		std::cout << tab[i] << " ";
	}
}
