#ifndef MYARRAY_H_
#define MYARRAY_H_
#include <iostream>
#include <exception>

using namespace std;

class myArray: public exception
{public:
	class memoryException
	{public:
		virtual const char* what() const throw ()
		{
			return "memory exception";
		}
	};

public:
	myArray();
	virtual ~myArray();

	int* at(int x);
	void resize(int new_size);
	void append(int element_to_add);
	void showUp();
private:
	int size;
	int* last_element;
	int* tab;
	int* tab2;
};

#endif
