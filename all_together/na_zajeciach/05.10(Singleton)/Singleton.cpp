#include "Singleton.h"
#include <iostream>

Singleton::Singleton()
{

}

Singleton::~Singleton()
{

}

Singleton& Singleton::getInstance()
{
	static Singleton *b = new Singleton;

	return *b;
}
