#ifndef DOG_H_
#define DOG_H_
#include "Animal.h"
#include <string>

class Dog: public Animal
{
public:
	Dog(string giveAName);
private:
	void giveASound();
};

#endif
