#ifndef FISH_H_
#define FISH_H_
#include <iostream>
#include "Animal.h"

class Fish
{
public:
	Fish(string giveAName);
	void speak();
private:
	string name;
};

#endif
