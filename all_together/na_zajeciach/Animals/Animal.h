#ifndef ANIMAL_H_
#define ANIMAL_H_
#include <string>

using namespace std;

class Animal
{
public:
	 Animal(string giveName);

	 void speak();
	 string giveAName(){return ("my name is: ") + name;};
protected:
	 virtual void giveASound()=0;	// =0 znaczy ze jest to metoda czysto wirtualna przez co klasa jest abstrakcyjna
	 string name;
};

#endif

