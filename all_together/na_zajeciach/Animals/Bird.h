#ifndef BIRD_H_
#define BIRD_H_

#include "Animal.h"

class Bird: public Animal
{
public:
	Bird(string n);
	virtual void fly()=0;
	virtual void giveASound()=0;
	string name;
};

#endif
