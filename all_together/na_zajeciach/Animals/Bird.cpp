#include "Bird.h"
#include <iostream>
#include "Animal.h"

Bird::Bird(string n):
Animal(n)
{

	std::cout << "Bird named: " << n << " has been created! " << std::endl;
}



