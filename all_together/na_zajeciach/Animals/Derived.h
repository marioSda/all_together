/*
 * Derived.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef DERIVED_H_
#define DERIVED_H_

#include "Base.h"

class Derived: public Base
{

public:
	using Base::f;
	using Base::g;   // udostepnianie funkcji z klasy Base przez co jest dostepna w klasie Derived tez ....

	Derived();
	void f();
	void g();
};

#endif /* DERIVED_H_ */
