#ifndef EAGLE_H_
#define EAGLE_H_

#include "Bird.h"

class Eagle: public Bird
{
public:
	Eagle(string n);
	void fly();
	void giveASound();
};

#endif
