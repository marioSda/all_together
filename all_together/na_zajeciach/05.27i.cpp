#include <iostream>
#include <vector>
using namespace std;

int main()
{

	vector<int> myVector;
	int temp=1;

	cout << "Put few random numbers:" << endl;
	while (true)
	{
		cin >> temp;
		if (!temp)
			break;
		else
			myVector.push_back(temp);

	}

	int total= 0;

	for(int i = 0; i < myVector.size(); i++)
	{

		total += myVector[i];
	}

	cout << "Total sum: " << total << endl;

	return 0;
}
