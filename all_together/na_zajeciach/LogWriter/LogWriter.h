#ifndef LOGWRITER_H_
#define LOGWRITER_H_
#include <fstream>
#include <iostream>
#include <string>


class LogWriter
{
public:
	LogWriter();
	virtual ~LogWriter();
	enum LogType{Fatal, Error, Warning, Info, Debug, Trace};

	void logMessage(std::string message, LogType messageType, bool decision);
	void setLoggingLevel(LogType logLevel){this->logLevel = logLevel;};

private:
	std::fstream f;
	LogType logLevel;

};

#endif
