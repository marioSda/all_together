#include "LogWriter.h"
#include <time.h>
#include <iostream>

LogWriter::LogWriter()
{
	f.open("co tam.txt", std::ios_base::app);
	logLevel = Error;
}

LogWriter::~LogWriter()
{
	f.close();
}

void LogWriter::logMessage(std::string message, LogType messageType, bool decision)
{
	time_t rawtime;
	time (&rawtime);
	if(messageType>logLevel && decision == true)
	{
	f << message << messageType;
	f << ctime(&rawtime);
	}else
	{
		std::cout << message << ctime(&rawtime) << std::endl;
	}
}


