//============================================================================
// Name        : 07e.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	// komputer nie jest w stanie odwzorowac 0.2 i wychodzi blad w zerze

	for(float i = -2; i <=2.2; i+=0.2)
	{
		cout << i << endl;
	}

	cout << endl;
	cout << "*****************************************\n" <<  endl;

	// rozwiazanie problemu

	for(int j = -20; j <= 20; j+=2)
	{
		cout << j/10.0 << endl;	// dzielimy przez 10.0 i int jest konwertowany domyslnie do double
	}

	return 0;
}
