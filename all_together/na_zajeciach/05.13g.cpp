
#include <iostream>
#include <cmath>

using namespace std;

double avarage(double ptr[])
{
	int counter = 0;
	double total = 0;
	int max = 12;

	for(int i = 0; i < max; i++)
	{
		total += ptr[i];
		counter++;
	}

	return total/counter;
}

int function(double ptr[])
{
	double avr = avarage(ptr);

	int temp = abs(avr-ptr[0]);
	int smallest = ptr[0];

	for(int i = 0; i < 12; i++)
	{
		if(abs(avr-ptr[i]) < temp)
		{
			temp = abs(avr-ptr[i]);
			smallest = ptr[i];
		}
	}

	return smallest;
}

int main() {

	double tab[]={1,2,3,4,5,6,7,8,9,10,10.75, 11.2};

	cout << avarage(tab) << endl;
	cout << function(tab) << endl;

	return 0;
}
