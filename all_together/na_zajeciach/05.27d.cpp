#include <iostream>
#include <vector>

using namespace std;

int main()
{

	vector<int> myVector(5);
	cout << myVector.capacity()<<endl;
	myVector.reserve(5);
	cout << myVector.capacity()<<endl;
	myVector.reserve(50);
	cout << myVector.capacity()<<endl;


	for (unsigned int i = 0; i < myVector.size(); i++)
	{
		cout << myVector.at(i) << " ";
	}

	cout << endl;

	myVector.resize(30, 50);

	for (unsigned int i = 0; i < myVector.size(); i++)
	{
		cout << myVector.at(i) << " ";
	}

	cout << endl;

	myVector.resize(3, 50);

		for (unsigned int i = 0; i < myVector.size(); i++)
		{
			cout << myVector.at(i) << " ";
		}

		cout << endl;

	for (unsigned int i = 0; i < myVector.size(); i++)
	{
		myVector.at(i) = 3;
	}

	cout << endl;

	vector<int>::iterator it = myVector.begin() + 2;

	myVector.insert(it, 5, 45);

	for (unsigned int i = 0; i < myVector.size(); i++)
	{
		cout << myVector.at(i) << " ";
	}

	cout << endl;

	myVector.erase(myVector.begin(), myVector.begin() + 2);

	for (unsigned int i = 0; i < myVector.size(); i++)
	{
		cout << myVector.at(i) << " ";
	}

	cout << endl;

	myVector.clear();
	cout << myVector.size() << endl;
	cout << "Is vector empty?: " << myVector.empty() << endl;


	for (unsigned int i = 0; i < myVector.size(); i++)
	{
		cout << myVector.at(i) << " ";
	}

	cout << endl;
	cout << "Final capacity: " << myVector.capacity() << endl;

	//cout << myVector.at(6) << endl;

	return 0;
}
