#include <iostream>
using namespace std;

int main()
{
	int tab[] =
	{ 9, 8, 7, 6, 5, 4, 3, 2, 1 };
	int temp;

	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (tab[j] > tab[j + 1])
			{
				temp = tab[j];
				tab[j] = tab[j + 1];
				tab[j + 1] = temp;
			}
		}
	}

	for (int i = 0; i < 9; i++)
	{
		cout << tab[i] << " ";
	}

	return 0;
}
