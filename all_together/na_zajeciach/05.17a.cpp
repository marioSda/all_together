#include <iostream>
#include <typeinfo>
#include <fstream>

using namespace std;

void displayException(exception& e)
{
	cout << "exception: " << e.what() << endl;
}

class Foo
{
public:
	virtual ~Foo();

};

class Bar
{
public:
	virtual ~Bar();
};

void badException() throw (bad_exception)
{
	throw runtime_error("moj wyjatek"); // bad_exception
}

int main()
{
//	Bar b;

	Bar *c = 0; // = new Bar...

	try
	{
//		for(int i = 0; ; i++)
//		{
//			int* wsk = new int[100000];	// bad_alloc
//		}
//		int* test = new (nothrow) int[10000000000]; // bez wyjatkow
//		if(test == 0)
//			cout << "nullptr" << endl;

//		Foo &f = dynamic_cast<Foo&>(b);

//		badException(); // bad_exception

//		cout << typeid(*c).name() << endl;

//		ifstream f("moj plik nie istnieje");
//		f.exceptions(f.failbit);

//		string text;	// out of range;
//		text.at(2);

//		string x;					// length error
//		x.resize(x.max_size()+1);


	} catch (bad_alloc& e)
	{
		displayException(e);

	} catch (bad_cast& e)
	{
		displayException(e);

	} catch (bad_exception& e)
	{
		displayException(e);

	} catch (bad_typeid& e)
	{
		displayException(e);

	} catch (length_error& e)
	{
		displayException(e);

	} catch (logic_error& e)
	{	// invalid_argument
		// domain_error
		// length_error
		displayException(e);
	} catch (ios_base::failure& e)
	{
		displayException(e);

	} catch (out_of_range& e)
	{
		displayException(e);
	} catch (...)
	{
		cout << "Unrecognized exception " << endl;
	}

	return 0;
}
