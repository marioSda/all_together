#include <iostream>
#include <vector>
using namespace std;

int main()
{

	vector<int> myVector;
	int temp=1;

	cout << "Put few random numbers:" << endl;
	while (true)
	{
		cin >> temp;
		if (!temp)
			break;
		else
			myVector.push_back(temp);

	}

	int min = myVector[0];
	int max = myVector[1];

	for(int i = 0; i < myVector.size(); i++)
	{

		cout << myVector[i] << " ";

		if(myVector[i]<min)
			min = myVector[i];
		if(myVector[i]>max)
			max = myVector[i];
	}


	cout << "\nMin: " << min;
	cout << "\nMax: " << max;

	return 0;
}
