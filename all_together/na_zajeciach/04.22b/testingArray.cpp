#include "Array.h"
#include "gtest.h"
#include <iostream>
#include <time.h>

class TestFixturesEmptyArray: public testing::Test
{
protected:

	time_t start_time;
	virtual void SetUp()
	{
		start_time = time(NULL);
	}

	virtual void TearDown()
	{
		const time_t end_time = time(NULL);
		EXPECT_TRUE(end_time - start_time <= 5);
	}

public:
	Array array;
	void addValue(int value){array.addValue(value);}
	void addValueAtIndex(int value, int index){array.addValueAtIndex(value,index);}
	void addMultipleValues(int amount){array.addMultipleValues(amount);}
	void fillInSmartArray(int size){array.fillInSmartArray(size);}
	void removeLastValue(){array.removeLastValue();}
	void removeValueAtIndex(int index){array.removeValueAtIndex(index);}
	void removeMultipleValues(int amount){array.removeMultipleValues(amount);}
	void emptySmartArray(){array.emptySmartArray();}
	void InsertionSort(){array.InsertionSort();}
	void reverseElements(){array.reverseElements();}

	int getAvarage(){return array.getAvarage();}
	int getMAX(){return array.getMAX();}
	int getMIN(){return array.getMIN();}
	int getLastValue(){return array.getLastValue();}
	int getSum(){return array.getSum();}
	int getCurrentVacant(){return array.getCurrentVacant();}
	int getValueAt(int index){return array.getValueAt(index);}
	int* firstElementPtr(){return array.firstElementPtr();}
	bool isEmpty(){return array.isEmpty();}
};

TEST_F(TestFixturesEmptyArray, adding_Single_Value)
{
	fillInSmartArray(1000);
	EXPECT_EQ(999, getLastValue());
	addValue(1000);
	EXPECT_NE(1000, getLastValue()); /// nie dodaje bo wykracza poza zakres
	emptySmartArray();
	addValue(1);
	EXPECT_EQ(1, getLastValue());
	addValue(-1);
	EXPECT_EQ(-1, getLastValue());
}

TEST_F(TestFixturesEmptyArray, adding_Value_At_Index)
{
	addValueAtIndex(20, 0);
	EXPECT_EQ(20, getLastValue());
	addValueAtIndex(20, 999);
	addValueAtIndex(20, 0);
	EXPECT_EQ(20, getValueAt(0));
	EXPECT_EQ(20, getValueAt(999));
	addValueAtIndex(20, 1000);		// wyswietlic blad ??


}

TEST_F(TestFixturesEmptyArray, filling_In_Array_by_Function)
{
	fillInSmartArray(1000);
	EXPECT_EQ(999, getLastValue());
	fillInSmartArray(1000);
	EXPECT_EQ(999, getLastValue());	// nadpisuje
	fillInSmartArray(2000);			// bez efektu
	fillInSmartArray(0);			// bez efektu
	fillInSmartArray(-1);			// bez efektu
}

TEST_F(TestFixturesEmptyArray, removing_Last_Element)
{
	removeLastValue();				// bez efektu
	EXPECT_EQ(0, getLastValue());
	EXPECT_EQ(0, getCurrentVacant());
	fillInSmartArray(50);
	removeLastValue();
	removeLastValue();
	EXPECT_EQ(47, getLastValue());
}

TEST_F(TestFixturesEmptyArray, removing_Particular_Element)
{
	fillInSmartArray(50);
	EXPECT_EQ(50, getCurrentVacant());
	removeValueAtIndex(0);
	EXPECT_EQ(49, getCurrentVacant());
	removeValueAtIndex(40);
	EXPECT_EQ(48, getCurrentVacant());
	EXPECT_EQ(0, getValueAt(100));
}

TEST_F(TestFixturesEmptyArray, emptying_Array)
{
	EXPECT_TRUE(isEmpty());
	fillInSmartArray(50);
	EXPECT_FALSE(isEmpty());
	emptySmartArray();
	EXPECT_TRUE(isEmpty());
}

TEST_F(TestFixturesEmptyArray, Filling_Array_And_Adding_Multiply_Values)
{
	EXPECT_EQ(0, getLastValue());
	addValue(20);
	EXPECT_EQ(20, getLastValue());
	fillInSmartArray(25);				// nadpis
	EXPECT_EQ(24, getLastValue());
	addMultipleValues(20);				// dodanie
	EXPECT_EQ(44, getLastValue());
	addMultipleValues(955);
	EXPECT_NE(999, getLastValue());	//	rozmiar sie nie zgadza --> nie dodano 995 bo to w sumie rowna sie MAX
	addMultipleValues(954);			// 	rozmiar sie zgadza
	EXPECT_EQ(998, getLastValue());
}

TEST_F(TestFixturesEmptyArray, summing_All_Elements_Up)
{
	EXPECT_EQ(0, getSum());
	addValue(20);
	EXPECT_EQ(20, getSum());
	addValue(20);
	addValue(20);
	EXPECT_EQ(60, getSum());
	removeLastValue();
	removeLastValue();
	removeLastValue();
	EXPECT_EQ(0, getSum());
}

TEST_F(TestFixturesEmptyArray, getting_Avarage_Value_of_all_elements)
{
	EXPECT_EQ(0, getAvarage());
	addValue(20);
	EXPECT_EQ(20, getAvarage());
	addValue(10);
	addValue(30);
	EXPECT_EQ(20, getAvarage());
	removeLastValue();
	removeLastValue();
	removeLastValue();
	EXPECT_EQ(0, getAvarage());
}

TEST_F(TestFixturesEmptyArray, getting_Min)
{
	EXPECT_EQ(0, getMIN());
	addValue(-1);
	EXPECT_EQ(-1, getMIN());
	addValue(1);
	addValue(-2);
	EXPECT_EQ(-2, getMIN());
}

TEST_F(TestFixturesEmptyArray, getting_Max)
{
	EXPECT_EQ(0, getMAX());
	addValue(20);
	addValue(10);
	EXPECT_EQ(20, getMAX());
	addValue(30);
	EXPECT_EQ(30, getMAX());
}

TEST_F(TestFixturesEmptyArray, sorting_by_Insertion)
{
	InsertionSort();
	fillInSmartArray(5);
	EXPECT_EQ(4, getValueAt(4));
	reverseElements();
	EXPECT_EQ(4, getValueAt(0));
	EXPECT_EQ(0, getValueAt(4));
	InsertionSort();
	EXPECT_EQ(0, getValueAt(0));
}

