#include "Array.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>

Array::Array()
{
	currentValues = 0;

//	srand(time(NULL));							// wypełnianie losowymi
//	for (int i = 0; i < currentSize; i++)
//	{
//		tab[i] = rand() % 100;
//	}
}

Array::Array(int limit)
{
	currentValues = 0;
	fillInSmartArray(limit);
}

Array::~Array()
{

}

void Array::addValue(int value)
{
	if (currentValues < MAX)
	{
		tab[currentValues] = value;
		currentValues++;
	}
}

void Array::addValueAtIndex(int value, int index)
{
	int temp;
	if (index > 0 && index < MAX)
	{
		for (int i = currentValues; i >= index; i--)
		{
			temp = tab[i];
			tab[i + 1] = temp;
		}
		tab[index] = value;
		currentValues++;
	}

	if(index == 0)
	{
		if(tab[index] == 0)
		{
			tab[index] = value;
			currentValues++;
		}else
		{
			temp = tab[index];
			tab[index + 1] = temp;
			tab[index]=value;
			currentValues++;
		}
	}
}

void Array::addMultipleValues(int amount)
{
	if(currentValues+amount<MAX)
	{
		for(int i = currentValues; i < currentValues+amount; i ++)
		{
			tab[i] = i;
		}
		currentValues += amount;
	}
}

void Array::fillInSmartArray(int size)
{
	if (size > 0 && size <= MAX)
	{
		for (int i = 0; i < size; i++)
		{
			tab[i] = i;
		}
		currentValues = size;
	}	// else zglos wyjatek, blad ?
};

void Array::removeLastValue()
{
	if(currentValues>0)
	{
		tab[currentValues] = 0;
		currentValues--;
	}
}

void Array::emptySmartArray()
{
	for(int i = 0; i < MAX; i++)
	{
		tab[i] = 0;
	}
	currentValues = 0;
}

bool Array::isEmpty()
{
	if(currentValues>0)
		return false;
	return true;
}


int Array::getAvarage()
{
	int total = 0;
	int avarage;

	if (currentValues == 0)
		return 0;

	for (int i = 0; i < currentValues; i++)
		total += tab[i];

	avarage = total / currentValues;
	return avarage;
}

int Array::getMAX()
{
	int Max = tab[0];

	for (int i = 0; i < currentValues; i++)
	{
		if (tab[i] > Max)
			Max = tab[i];
	}

	return Max;
}

int Array::getMIN()
{
	int Min = tab[0];

	for (int i = 0; i < currentValues; i++)
	{
		if (tab[i] < Min)
			Min = tab[i];
	}

	return Min;
}

int Array::getSum()
{
	int total = 0;

	for (int i = 0; i < currentValues; i++)
	{
		total += tab[i];
	}

	return total;
}

int Array::getLastValue()
{
	return tab[currentValues-1];
}

int Array::getCurrentVacant()
{
	return currentValues;
}

int Array::getValueAt(int index)
{
	return tab[index];
}

void Array::removeValueAtIndex(int index)
{
	if (index >= 0 && index <= MAX)
	{
		int temp;
		for (int i = index + 1; i < currentValues; i++)
		{
			temp = tab[i];
			tab[i - 1] = temp;
		}
		currentValues--;
	}
}

void Array::InsertionSort()
{
	if (currentValues >= 2)
	{
		for (int j = currentValues - 2; j >= 0; j--)
		{
			int temp = tab[j];
			int i = j + 1;
			while ((i < currentValues) && (temp > tab[i]))
			{
				tab[i - 1] = tab[i];
				i++;
			}
			tab[i - 1] = temp;
		}
	}
}

int* Array::firstElementPtr()
{
	return &tab[0];
}

void Array::setCurrentSize(int size)
{
	currentValues = size;
}

void Array::reverseElements()
{

	int tab2[currentValues]={};

	int counter = currentValues - 1;

	for (int i = 0; i < currentValues; i++)
	{
		tab2[i] = tab[counter];
		counter--;
	}

	for (int i = 0; i < currentValues; i++)
		tab[i] = tab2[i];
}

void Array::removeMultipleValues(int amount)
{
	if(currentValues-amount>0)
	{
		for(int i = currentValues-1; i > (currentValues-1)-amount; i--)
		{
			tab[i] = 0;
		}
		currentValues -= amount;
	}
}
