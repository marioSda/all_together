#include "Array.h"
#include "gtest.h"

class TestFixturesAutoFillArray: public testing::Test
{
protected:

	virtual void SetUp()
	{
		fillInSmartArray(20);
		testObject.fillInSmartArray(20);
	}

	virtual void TearDown()
	{

	}

public:
	Array array;
	Array testObject;
	Array testObject2;
	void addValue(int value){array.addValue(value);}
	void addValueAtIndex(int value, int index){array.addValueAtIndex(value,index);}
	void addMultipleValues(int amount){array.addMultipleValues(amount);}
	void fillInSmartArray(int size){array.fillInSmartArray(size);}
	void removeLastValue(){array.removeLastValue();}
	void removeValueAtIndex(int index){array.removeValueAtIndex(index);}
	void removeMultipleValues(int amount){array.removeMultipleValues(amount);}
	void emptySmartArray(){array.emptySmartArray();}
	void InsertionSort(){array.InsertionSort();}
	void reverseElements(){array.reverseElements();}

	int getAvarage(){return array.getAvarage();}
	int getMAX(){return array.getMAX();}
	int getMIN(){return array.getMIN();}
	int getLastValue(){return array.getLastValue();}
	int getSum(){return array.getSum();}
	int getCurrentVacant(){return array.getCurrentVacant();}
	int getValueAt(int index){return array.getValueAt(index);}
	int* firstElementPtr(){return array.firstElementPtr();}
	bool isEmpty(){return array.isEmpty();}

};

TEST_F(TestFixturesAutoFillArray, sorting_by_Insertion)
{
	EXPECT_EQ(19, getLastValue());
	reverseElements();
	EXPECT_EQ(19, *firstElementPtr());
	InsertionSort();
}

TEST_F(TestFixturesAutoFillArray, another_Object)
{
	addMultipleValues(20);
	EXPECT_EQ(39, getLastValue());
	EXPECT_EQ(19, testObject.getLastValue());
	EXPECT_EQ(0, testObject2.getLastValue());
	Array testObject3(100);
	EXPECT_EQ(99, testObject3.getLastValue());
}

TEST_F(TestFixturesAutoFillArray, deletingMultipleValues)
{
	EXPECT_EQ(19, getLastValue());
	EXPECT_EQ(20, getCurrentVacant());
	removeMultipleValues(2);
	EXPECT_EQ(17, getLastValue());
	EXPECT_EQ(18, getCurrentVacant());
	EXPECT_EQ(0, getValueAt(25));

}

TEST_F(TestFixturesAutoFillArray, adding_Value_At_Index_Full)
{
	addValueAtIndex(20, 20);
	EXPECT_EQ(20, getLastValue());

}
