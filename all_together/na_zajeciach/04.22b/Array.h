#ifndef ARRAY_H_
#define ARRAY_H_

const int MAX = 1000;

class Array
{
public:
	Array();
	Array(int limit);
	virtual ~Array();

	void addValue(int value);
	void addValueAtIndex(int value, int index);
	void addMultipleValues(int amount);     // dodaje od pierwszej wolnej pozycji
	void fillInSmartArray(int size);		// wypelnia tablice od poczatku nadpisujac zajete juz pola nowymi elementami
	void removeLastValue();
	void removeValueAtIndex(int index);
	void removeMultipleValues(int amount);
	void emptySmartArray();
	void InsertionSort();
	void reverseElements();

	int getAvarage();
	int getMAX();
	int getMIN();
	int getLastValue();
	int getSum();
	int getCurrentVacant();
	int getValueAt(int index);
	int* firstElementPtr();
	void setCurrentSize(int size);



	bool isEmpty();

private:

	int currentValues;
	int tab[MAX]={};

};

#endif
