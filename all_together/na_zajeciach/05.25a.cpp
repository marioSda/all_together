#include <iostream>
using namespace std;


int solution(int* A, int size_of_A)
{

	int length1 = 0;
	int length2 = 0;

	int index;
	for(int i = 0, j = i+1; j < size_of_A; i++, j++)
	{
		if(A[i]<A[j])
		{
			length1++;

			if(length2 < length1)
			{
				length2 = length1;
				index = i - length1+1;
			}

		}else
		{
			if(length2 < length1)
			{
				length2 = length1;
				index = i - length1;
			}

			length1 = 0;

		}
	}

	return index;
}


int main() {

	const int size = 18;
	int tab[size] = {2,2,2,2,1,2,3,-1,2,3,0,1,3,4,5,6,7,8};				/// koniec najdluzszy
	int tab2[size] = {0,1,2,3,4,5,6,-1,2,3,0,0,0,0,5,6,7,8};			/// poczatek najdluzszy
	int tab3[size] = {0,1,2,3,4,-1,0,1,2,3,4,5,0,0,5,6,7,8};			/// srodek najdluzszy
	int tab4[size] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17};		/// ca;y

	cout << solution(tab4,size) << endl;







	return 0;
}
