#include <iostream>
#include <string>
#include <sstream>

using namespace std;

string solution(string &S)
{
	istringstream iss(S);
	string sub;
	string final;

	while (iss)
	{
		iss >> sub;

		string copy = sub;

		int j = sub.length() - 1;
		for (int i = 0; i < sub.length(); i++)
		{
			sub[i] = copy[j];
			j--;
		}

		final += sub;
		final += ' ';
	}

	return final;
}

int main()
{

	string random = "we test coders";
	string abc = "abc";

	cout << solution(random) << endl;

	return 0;
}
