#include <iostream>
using namespace std;

bool check(char* a, char* b)
{
	int len = 0;

		while(*a++!=0)
			len++;

	char random[len];

	int counter = 0;
	for(int i = 0; i < len; i++)
	{
		random[counter] = *b;
		b--;
		counter++;
	}
	b+=counter;
	counter=0;

	for (int i = 0; i < len; i++)
	{
		if (*a == *b)
			counter++;

		a++;
		b--;
	}

	if (counter == len)
		return true;

	return false;
}

int main()
{
	char a[] = "abccba";

	char* poczatek = &a[0];
	char* koniec = &a[5];


	cout << check(poczatek, koniec) << endl;

	return 0;
}
