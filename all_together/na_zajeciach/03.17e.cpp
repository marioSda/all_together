#include <iostream>
using namespace std;
/// iloczyn skalarny wektorow /////
int iloczynSkalarny(int *p1, int *p2, int n);

int main() {

	int tab1[3]={2,3,4};
	int tab2[3]={5,6,7};

	int *pTab1=tab1;
	int *pTab2=tab2;

	cout<<iloczynSkalarny(pTab1, pTab2, 3)<<endl;

	return 0;
}

int iloczynSkalarny(int *p1, int *p2, int n)
{
	int il=0;
	for(int i = 0; i < n; i++,p2++,p1++)
		il+=*p2*(*p1);

	return il;
}
