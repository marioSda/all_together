#include <iostream>
using namespace std;

int main()
{
	int v1, v2;

	cout << "Podaj wspolrzedne wektora 1: ";
	cin >> v1 >> v2;
	int vector1[2] = {v1, v2};

	cout << "Podaj wspolrzedne wektora 2: ";
	cin >> v1 >> v2;

	int vector2[2] = {v1, v2};

	cout << "Wspolrzedne wektora 1: ";
	for (int i = 0; i < 2; i++)
		cout << vector1[i] << " ";

	cout << endl;

	cout << "Wspolrzedne wektora 2: ";
	for (int i = 0; i < 2; i++)
		cout << vector2[i] << " ";

	cout << endl;

	int suma[2] = {};

	for (int i = 0; i < 2; i++)
		suma[i] = vector1[i] + vector2[i];

	cout << "Wspolrzedne wektora bedacego suma podanych wynosza: ";

	for (int j = 0; j < 2; j++)
		cout << suma[j] << " ";

	return 0;
}
