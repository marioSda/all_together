#include <iostream>
using namespace std;

struct Lista
{
	Lista* next;
	char data;
};

unsigned rozmiar(Lista* a);
void push_front(Lista* &head, char v);
void pop_front(Lista* &head);
void wyswietl(Lista* a);


int main() {

Lista* L = 0;

push_front(L, 'H');
push_front(L, 'A');
push_front(L, 'H');
wyswietl(L);
pop_front(L);
pop_front(L);
wyswietl(L);

	return 0;
}

unsigned rozmiar(Lista* a)
{
	unsigned licznik = 0;

	while(a)
	{
		licznik++;
		a = a->next;
	}
	return licznik;
}

void wyswietl(Lista* a)
{
	unsigned x;

	cout << "Number of elements : " << rozmiar(a) << endl;

	for(int x = 1; a; a = a->next)
		cout << "Element " << x++ << " data = " << a->data << endl;
	cout << endl;
}

void push_front(Lista* &head, char v)
{
	Lista* p;

	p = new Lista;
	p->data = v;
	p->next = head;
	head = p;
}


void pop_front(Lista* &head)
{
	Lista* p = head;

	if(p)
	{
		head = p->next;
		delete p;
	}
}

