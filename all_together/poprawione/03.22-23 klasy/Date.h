#ifndef DATA_H
#define DATA_H

class Date
{
    int year;
    int month;
    int day;

public:

    Date(int d=22, int m=3, int r=2017);

    int getRok()const;
    int getMiesiac()const;
    int getDzien()const;
    void setYear(int y);
    void setMonth(int m);
    void setDay(int d);
    void isValid();
    bool isInLappYear();
    int daysTillTheEndOfTheMonth();
    int daysTillTheEndOfTheYear();
    void addDay();
    void addMonth();
    void addYear();
    void addDays(int d);
    void addMonths(int m);
    void addYears(int y);
    bool operator==(const Date& other)const;
    bool operator!=(const Date& other)const;
    bool operator>(const Date& other)const;
};

#endif
