#include <iostream>
#include "Date.h"
using namespace std;

	/// Program operujacy na datach ///

int main()
{
    Date data1(22,03,2017);
    data1.isValid();
    Date data2(22,03,2017);
    data1.isValid();

    cout << "Days till the end of the year: " << data1.daysTillTheEndOfTheYear() << endl;
    cout << "Days till the end of the month: " << data1.daysTillTheEndOfTheMonth() << endl;

    /// POROWNYWANIE DWOCH OBIEKTOW ///

    //cout << data1.operator ==(data2) << endl;
    cout << "Are these objects equal? " << (data1 == data2) << endl;
    cout << "Is one object bigger than the other one? " << (data1 > data2) << endl;

    return 0;
}
