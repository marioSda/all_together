#include "Date.h"
#include <iostream>
using namespace std;

const int size = 12;
int leapYear[size] ={ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
int normalYear[size] ={ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

Date::Date(int d, int m, int r)
{
	year = r;
	month = m;
	day = d;
}

int Date::getRok() const{return year;}
int Date::getMiesiac() const{return month;}
int Date::getDzien() const{return day;}

void Date::setYear(int y){year = y;}
void Date::setMonth(int m){month = m;}
void Date::setDay(int d){day = d;}

bool Date::isInLappYear()
{
	if (((year % 4) == 0 && (year % 100) != 0 && year != 0) || (year % 400) == 0)
		return 1;
	return 0;
}

void Date::isValid()
{
	switch (month)
	{
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
	{
		if (day < 1 || day > 31)
			cout << "Wrong amount of days!" << endl;

		if (year == 0)
			cout << "Wrong year! " << endl;

		if (isInLappYear())
			cout << "(leap year)" << endl;
		else
			cout << "(not a leap year)" << endl;

		break;
	}
	case 2:
	{
		if (isInLappYear() == false && (day < 1 || day >= 29))
			cout << "Wrong amount of days!" << endl;
		else if (isInLappYear() && (day < 1 || day > 29))
			cout << "Wrong amount of days!" << endl;

		if (year == 0)
			cout << "Wrong year! " << endl;

		if (isInLappYear())
			cout << "(leap year)" << endl;
		else
			cout << "(not a leap year)" << endl;

		break;
	}
	case 4:
	case 6:
	case 9:
	case 11:
	{
		if (day < 1 || day >= 30)
			cout << "Wrong amount of days!" << endl;

		if (year == 0)
			cout << "Wrong year! " << endl;

		if (isInLappYear())
			cout << "(leap year)" << endl;
		else
			cout << "(not a leap year)" << endl;

		break;
	}
	default:
		cout << "Wrong month! " << endl;
		break;
	}

	cout << day << "/" << month << "/" << year << endl;
	cout << endl;
}

int Date::daysTillTheEndOfTheMonth()
{
	int total(0);

	if (isInLappYear())
		total = leapYear[month - 1] - day;
	else
		total = normalYear[month - 1] - day;

	return total;
}

int Date::daysTillTheEndOfTheYear()
{
	int total(0);

	if (isInLappYear())
	{
		total = leapYear[month - 1] - day;
		for (int i = month; i < size; i++)
		{
			total += leapYear[i];
		}
	}
	else
	{
		total = normalYear[month - 1] - day;
		for (int i = month; i < size; i++)
		{
			total += normalYear[i];
		}
	}
	return total;
}

void Date::addDay()
{
	if (isInLappYear())
	{
		if ((day += 1) > leapYear[month - 1])
		{
			day = 1;
			month += 1;
		}
	}
	else
	{
		if ((day += 1) > normalYear[month - 1])
		{
			day = 1;
			month += 1;
		}
	}
}

void Date::addMonth()
{
	if ((month += 1) > size)
	{
		year += 1;
		month = 1;
	}
}

void Date::addYear()
{
	year += 1;
	if (!isInLappYear())
	{
		if (month == 2 && day > 28)
		{
			month += 1;
			day = 1;
		}
	}
}

void Date::addDays(int d)
{
	int days;
	days = (day += d);

	if (isInLappYear())
	{
		while ((days) > leapYear[month - 1])
		{
			days -= leapYear[month - 1];
			month++;
		}
		day = days;
	}
	else
	{
		while ((days) > normalYear[month - 1])
		{
			days -= normalYear[month - 1];
			month++;
		}
		day = days;
	}
}

void Date::addMonths(int m)
{
	int months = (month += m);

	while (months > size)
	{
		months -= size;
		year++;
	}
	month = months;
}

void Date::addYears(int y)
{
	year += y;
	if (!isInLappYear())
	{
		if (month == 2 && day > 28)
		{
			month += 1;
			day = 1;
		}
	}
}

bool Date::operator==(const Date& other) const
{
	if (day == other.day && month == other.month && year == other.year)
		return true;
	return false;
}

bool Date::operator!=(const Date& other) const
{
	return !operator==(other);
	//return !(*this == other);

}

bool Date::operator>(const Date& other) const
{
	if (year > other.year)
	{
		return true;
	}
	else if (year == other.year)
	{
		if (month > other.month)
		{
			return true;
		}
		else if (month == other.month)
		{
			if (day > other.day)
			{
				return true;
			}
		}
	}
	return false;
}
