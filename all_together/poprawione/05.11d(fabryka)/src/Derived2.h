#ifndef DERIVED2_H_
#define DERIVED2_H_

#include "Base.h"

class Derived2: public Base
{
public:
	Derived2();
	virtual ~Derived2();

	void makeSomething(){std::cout << "Derived2 Class is making something " << std::endl;}
};

#endif
