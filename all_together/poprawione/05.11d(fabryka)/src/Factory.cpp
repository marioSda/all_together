#include "Factory.h"

Factory::Factory()
{

}

Factory::~Factory()
{

}

Base* Factory::createObjectOf(const std::string &ClassName)
{
	if(ClassName=="Base")
		return new Base;
	else if(ClassName=="Derived1")
		return new Derived1;
	else if(ClassName=="Derived2")
		return new Derived2;

	return 0;

}
