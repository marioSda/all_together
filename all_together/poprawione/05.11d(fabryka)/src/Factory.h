#ifndef FACTORY_H_
#define FACTORY_H_

#include <string>
#include "Base.h"
#include "Derived1.h"
#include "Derived2.h"


class Factory
{
public:
	Factory();
	virtual ~Factory();

	static Base* createObjectOf(const std::string &ClassName);

};

#endif
