#ifndef DERIVED1_H_
#define DERIVED1_H_

#include "Base.h"

class Derived1: public Base
{
public:
	Derived1();
	virtual ~Derived1();

	void makeSomething(){std::cout << "Derived1 Class is making something " << std::endl;}


};

#endif
