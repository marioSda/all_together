#ifndef BASE_H_
#define BASE_H_

#include <iostream>

class Base
{
public:
	Base();
	virtual ~Base();

	virtual void makeSomething(){std::cout << "Base Class is making something " << std::endl;}
};

#endif
