#include <iostream>
#include "Base.h"
#include "Factory.h"

using namespace std;

int main() {

	Base* tab[5];

	tab[0] = Factory::createObjectOf("Base");
	tab[1] = Factory::createObjectOf("Derived1");
	tab[2] = Factory::createObjectOf("Derived2");
	tab[3] = Factory::createObjectOf("Base");
	tab[4] = Factory::createObjectOf("Derived1");

	for(int i = 0; i < 5; i++)
	{
		tab[i]->makeSomething();
	}

	return 0;
}
