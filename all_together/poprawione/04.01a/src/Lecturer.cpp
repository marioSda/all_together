#include "Lecturer.h"
#include <iostream>
#include  <string>


Lecturer::Lecturer()
{

}

Lecturer::Lecturer(std::string sNam, std::string sSur, int sAge,
		std::string sMail) :
		Person(sNam, sSur, sAge, sMail)
{
	wsk3 = 0;
}

void Lecturer::coursesProvided()
{
	std::cout << "Lecturer " << getName() << " " << getSurname()
			<< ", has assigned courses: " << wsk3 << std::endl;
	for (int i = 0; i < wsk3; i++)
	{
		std::cout << i + 1 << " - " << lecturerCourses[i] << std::endl;
	}
}

void Lecturer::description()
{
	std::cout << "Lecturer: " << getName() << " " << getSurname() << "\n"
			<< "age: " << getAge() << "\n" << "mail: " << getMail()
			<< std::endl;
	std::cout<<std::endl;
}

void Lecturer::addCourse(std::string course)
{
	if (wsk3 == 10)
		std::cout << "No more courses can be added!" << std::endl;

	while (wsk3 != size3)
	{
		lecturerCourses[wsk3] = course;
		wsk3++;
		break;
	}
}
