#ifndef ROOM_H_
#define ROOM_H_

#include <string>
#include <iostream>


int const size=10;

class Room
{
public:
	Room();
	Room(int no, int cap, int b);

	void description();
	void addCourse(std::string course);
	void coursesInRoom();

	int getBuilding() const{return building;}
	int getCapacity() {return capacity;}
	int getNumber() const{return number;}

	void setBuilding(int buildingNr){building = buildingNr;}
	void setCapacity(int RoomCapacity){capacity = RoomCapacity;}
	void setNumber(int roomNr){number = roomNr;}

private:
	int number;
	int capacity;
	int building;
	std::string courses[size];
	int wsk;


};

#endif
