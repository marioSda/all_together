#include <iostream>
#include "Person.h"
#include "Room.h"
#include "Student.h"
#include "Lecturer.h"
#include "Course.h"

using namespace std;

int main()
{

	Student s1("Mariusz", "Swietojanski", 28, "mariusz.swietojanski@gmail.com");
	Student s2("msm", "sad", 2, "ads@o.pl");
	Student s3("Kuba", "Ktos", 30, "mama@o3.pl");
	Student s4("Tomek", "Tomaszewski", 22, "ows@o2.pl");
	Student s5("Wojtek", "Swiatek", 34, "oe2@ow.pl");
	Student s6("Bartek", "fdsfd", 33, "9029@op.pl");

	int licznikStudent = 6;
	Student tab[licznikStudent] = { s1, s2, s3, s4, s5, s6 };

	Room r1(1, 100, 5);
	Room r2(2, 100, 5);
	Room r3(3, 100, 5);
	Room r4(4, 100, 5);

	int licznikRoom = 4;
	Room tab2[licznikRoom] = { r1, r2, r3, r4 };

	Lecturer l1("Pawel", "Pawlak", 50, "pp@o2.pl");
	Lecturer l2("Wojtek", "Cieplak", 20, "wc@o2.pl");


	Course course1("01.01.2017", "01.03.2017", "Mathematics", 20);
	course1.enrolStudent(&s1);
	course1.enrolStudent(&s2);
	course1.assignRoom(&r1);
	course1.enrolLecturer(&l1);
	course1.enrolLecturer(&l2);
	course1.getLecturer();
	course1.getStudents();









	return 0;
}
