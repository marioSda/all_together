#ifndef COURSE_H_
#define COURSE_H_

#include "Student.h"
#include "Lecturer.h"
#include "Room.h"
#include <iostream>
#include <string>

class Course
{
public:
	Course();
	Course(std::string stDate, std::string finDate, std::string top, int plc);

	void description();
	void enrolStudent(Student* stud);
	void enrolLecturer(Lecturer* lect);
	void assignRoom(Room* room);

	void getLecturer();
	void getStudents();

private:

	std::string courses[10];

	Room* roomForCourse;
	Lecturer* lecturers[2];
	Student* students[20];

	std::string startDate;
	std::string finishDate;
	std::string topic;
	int places;
	int licznik;
	int licznik2;



};

#endif
