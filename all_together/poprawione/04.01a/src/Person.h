#ifndef PERSON_H_
#define PERSON_H_

#include <string>
#include <iostream>


class Person
{
public:
	Person();
	Person(std::string sNam, std::string sSur, int sAge, std::string sMail);

	int getAge() const{return age;}
	const std::string getMail() const{return mail;}
	const std::string getName() const{return name;}
	const std::string getSurname() const{return surname;}

	void setAge(int a){age = a;}
	void setMail(const std::string m){mail = m;}
	void setName(const std::string n){name = n;}
	void setSurname(const std::string s){surname = s;}

private:
	std::string name;
	std::string surname;
	int age;
	std::string mail;


};

#endif
