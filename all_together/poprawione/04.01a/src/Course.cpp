#include "Course.h"
#include <string>
#include <iostream>

Course::Course()
{

}

Course::Course(std::string stDate, std::string finDate, std::string top,
		int plc)
{
	startDate = stDate;
	finishDate = finDate;
	topic = top;
	places = plc;
	licznik = 0;
	licznik2 = 0;
	roomForCourse = 0;
}

void Course::description()
{
	std::cout << "Course: " << topic << "\n" << "Start date: " << startDate
			<< "\n" << "Finish date: " << finishDate << "\n" << "Places: "
			<< places << "\n" << "Vacant plc: " << places - licznik
			<< std::endl;
}

void Course::enrolStudent(Student* stud)
{
	if (licznik < 20)
	{
		students[licznik] = stud;
		licznik++;
	}
	else
	{
		std::cout << "Course is full! No more vacant places" << std::endl;
	}
}

void Course::enrolLecturer(Lecturer* lect)
{
	if (licznik2 < 2)
	{
		lecturers[licznik2] = lect;
		licznik2++;
	}
	else
	{
		std::cout << "There's already two lecturers! Enough" << std::endl;
	}
}

void Course::assignRoom(Room* room)
{
	roomForCourse = room;
}

void Course::getLecturer()
{
	std::cout << "Lecturers providing course " << topic << " : \n" << std::endl;
	for (int i = 0; i < licznik2; i++)
	{
		std::cout << i+1 << " - ";
		lecturers[i]->description();
	}
}

void Course::getStudents()
{
	std::cout << "Students enrolled to course " << topic << " : \n" << std::endl;
	for (int i = 0; i < licznik; i++)
	{
		std::cout << i+1 << " - ";
		students[i]->description();
	}
}
