#ifndef STUDENT_H_
#define STUDENT_H_

#include "Person.h"
int const size2 = 10;

class Student: public Person
{
public:
	Student();
	Student(std::string sNam, std::string sSur, int sAge, std::string sMail);

	void coursesAttended();
	void description();
	void assignToCourse(std::string course);

private:
	std::string studentCourses[size2];
	int wsk2;

};

#endif

