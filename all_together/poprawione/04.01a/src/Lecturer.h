#ifndef LECTURER_H_
#define LECTURER_H_

#include "Person.h"
int const size3(3);

class Lecturer: public Person
{
public:
	Lecturer();
	Lecturer(std::string sNam, std::string sSur, int sAge, std::string sMail);

	void coursesProvided();
	void description();
	void addCourse(std::string course);

private:
	std::string lecturerCourses[size3];
	int wsk3;

};

#endif
