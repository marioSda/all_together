#ifndef SINGLETON_H_
#define SINGLETON_H_

class Singleton {
public:

	static Singleton* getInstance();

private:
	Singleton();
	virtual ~Singleton();

	static Singleton* wsk;

};

#endif
