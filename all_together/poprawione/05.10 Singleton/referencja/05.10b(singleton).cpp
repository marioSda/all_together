#include <iostream>
#include "Singleton.h"
using namespace std;

int main() {

	cout << &Singleton::getInstance() << endl;

	Singleton* single1;
	cout << &single1->getInstance() << endl;
	Singleton* single2;
	cout << &single2->getInstance() << endl;

	return 0;
}
