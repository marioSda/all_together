#include "Singleton.h"

Singleton::Singleton()
{

}

Singleton::~Singleton()
{

}

Singleton& Singleton::getInstance()
{
	static Singleton *b = new Singleton;

	return *b;
}
