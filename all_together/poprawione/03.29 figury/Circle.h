#ifndef CIRCLE_H
#define CIRCLE_H

#include "Shape.h"
#include <string>

class Circle : public Shape
{
public:
    Circle(std::string n, double r=10);
    void draw();
    void description();
    double area(){return 3.14*radius*radius;};
    double getRadius(){return radius;};

private:
    double radius;

};

#endif
