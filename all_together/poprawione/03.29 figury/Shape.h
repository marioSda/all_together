#ifndef SHAPE_H
#define SHAPE_H

#include <string>

class Shape
{
public:
    Shape(std::string n);
    virtual void draw()=0;
    virtual void description()=0;
    virtual double area()=0;
    std::string getName(){return name;};

private:
    std::string name;

};

#endif
