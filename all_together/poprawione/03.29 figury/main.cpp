#include <iostream>
#include "Shape.h"
#include "Square.h"
#include "Triangle.h"
#include "Circle.h"

using namespace std;

int main()
{
    Square square("kwadrat", 10);
    Triangle triangle("trojkat", 10, 10);
    Circle circle("kolo", 10);

    Shape* tab[3] = {&square, &triangle, &circle};

    for(int i = 0; i < 3; i++)
    {
        tab[i]->description();
    }

    return 0;
}
