#include "Square.h"
#include <iostream>
#include <string>

Square::Square(std::string n, double s):
    Shape(n)
{
    side = s;
}

void Square::draw()
{
    for(int i = 0; i < side; i++)
    {
        for(int j = 0; j < side; j++)
        {
            std::cout << " * ";
        }
        std::cout<<std::endl;
    }
}

void Square::description()
{
    std::cout << "SQUARE\n"
              << "=========\n"
              << "-name: "
              << getName() << "\n"
              << "-side: "
              << getSide() << "\n"
              << "-area: "
              << area() << "\n"
              << "-example: "
              << std::endl;
    draw();
}



