#include "Triangle.h"
#include <iostream>
#include <string>

Triangle::Triangle(std::string n, double b, double h):
    Shape(n)
{
    base = b;
    height = h;
}

void Triangle::draw()
{
    for(int i = 0; i < base; i++)
    {
        for(int j = 0; j <= i; j++)
        {
            std::cout << " * ";
        }
        std::cout<<std::endl;
    }
}

void Triangle::description()
{
    std::cout << "TRIANGLE\n"
              << "=========\n"
              << "-name: "
              << getName() << "\n"
              << "-base: "
              << getBase() << "\n"
              << "-height: "
              << getHeight() << "\n"
              << "-area: "
              << area() << "\n"
              << "-example: "
              << std::endl;
    draw();
}



