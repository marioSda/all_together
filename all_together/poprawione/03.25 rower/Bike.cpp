#include "Bike.h"
#include "Frame.h"
#include "Wheels.h"
#include "Lights.h"
#include "Acceleration.h"
#include <iostream>
using namespace std;

int choice;

Bike::Bike()
{
	cout << "...Default bike is ready!" << endl;
	cout << "*****************" << endl;
}

void Bike::accelerateOne()
{
	if ((acceleration.getFrontGear() == 3) && acceleration.getBackGear() == 7)
		cout << "Can't go any faster!" << endl;
	else
	{
		acceleration.backGearUp();
		cout << "One gear up! (F:" << acceleration.getFrontGear() << ", B:"
				<< acceleration.getBackGear() << ")" << endl;
	}
}

void Bike::accelerateFew(int few)
{
	int gears(
			21
					- (acceleration.getBackGear()
							+ (acceleration.getFrontGear() - 1) * 7));

	if (few >= gears)
		cout << "Too many gears up! Above the limit!\nCan't go any faster!"
				<< endl;
	else
	{
		acceleration.backGearUp(few);
		cout << few << " gears up! (F:" << acceleration.getFrontGear() << ", B:"
				<< acceleration.getBackGear() << ")" << endl;
	}
}

void Bike::brakeOne()
{
	if ((acceleration.getFrontGear() == 1) && acceleration.getBackGear() == 1)
		cout << "Almost stopped!" << endl;
	else
	{
		acceleration.backGearDown();
		cout << "One gear down! (F:" << acceleration.getFrontGear() << ", B:"
				<< acceleration.getBackGear() << ")" << endl;
	}
}

void Bike::brakeFew(int few)
{
	int gears(
			acceleration.getBackGear() + (acceleration.getFrontGear() - 1) * 7);

	if (few >= gears)
		cout << "Too many gears down! Below the limit!" << endl;
	else
	{
		acceleration.backGearDown(few);
		cout << few << " gears down! (F:" << acceleration.getFrontGear()
				<< ", B:" << acceleration.getBackGear() << ")" << endl;
	}

	if ((acceleration.getFrontGear() == 1) && acceleration.getBackGear() == 1)
		cout << "Almost stopped!" << endl;
}
void Bike::stop()
{
	acceleration.setFrontGear(1);
	acceleration.setBackGear(1);
	cout << "Bike has stopped!\n";
}

void Bike::turnOnLghts()
{
	lights.frontlightOn();
	lights.backlightOn();
}
void Bike::turnOffLghts()
{
	lights.frontlightOff();
	lights.backlightOff();
}

void Bike::changeFrame()
{
	int choice2;
	string size;
	cout << "Frames you can choose from:\n"
			"1. hardtail\n"
			"2. road\n"
			"3. sport\n"
			"4. light\n"
			"**********\n"
			"What size of a frame?\n"
			"1. small\n"
			"2. medium\n"
			"3. big\n";

	cin >> choice >> choice2;

	switch (choice2)
	{
	case 1:
		frame.setSize(Frame::small);
		size = "(small)";
		break;
	case 2:
		frame.setSize(Frame::medium);
		size = "(medium)";
		break;
	case 3:
		frame.setSize(Frame::large);
		size = "(big)";
		break;
	default:
		cout << "Wrong choice!" << endl;
		break;
	}

	switch (choice)
	{
	case 1:
		frame.setType(Frame::hardtail);
		cout << "Hardtail " << size << " frame installed!\n" << endl;
		break;
	case 2:
		frame.setType(Frame::road);
		cout << "Road type " << size << " frame installed!\n" << endl;
		break;
	case 3:
		frame.setType(Frame::sport);
		cout << "Sport type " << size << " frame installed!\n" << endl;
		break;
	case 4:
		frame.setType(Frame::light);
		cout << "Light " << size << " frame installed!\n" << endl;
		break;
	default:
		cout << "Wrong choice!" << endl;
		break;
	}

}
void Bike::changeSizeWheels()
{
	cout << "Sizes of wheels you can choose from:\n"
			"1. small\n"
			"2. medium\n"
			"3. big\n";

	cin >> choice;

	switch (choice)
	{
	case 1:
		wheels.setSize(Wheels::small);
		cout << "Small wheels installed!\n" << endl;
		break;
	case 2:
		wheels.setSize(Wheels::medium);
		cout << "Medium wheels installed!\n" << endl;
		break;
	case 3:
		wheels.setSize(Wheels::big);
		cout << "Big wheels installed!\n" << endl;
		break;
	default:
		cout << "Wrong choice!" << endl;
		break;
	}
}
void Bike::changeTires()
{
	cout << "Tires you can choose from:\n"
			"1. mountain\n"
			"2. road\n"
			"3. sport\n";

	cin >> choice;

	switch (choice)
	{
	case 1:
		wheels.setTire(Wheels::mountain);
		cout << "Mountain tires installed!\n" << endl;
		break;
	case 2:
		wheels.setTire(Wheels::road);
		cout << "Road tires installed!\n" << endl;
		break;
	case 3:
		wheels.setTire(Wheels::sport);
		cout << "Sport tires installed!\n" << endl;
		break;
	default:
		cout << "Wrong choice!" << endl;
		break;
	}
}
