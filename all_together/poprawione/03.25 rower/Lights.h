#ifndef LIGHTS_H_
#define LIGHTS_H_

class Lights
{
public:

	Lights(bool front=false, bool back=false);
	void frontlightOn();
	void frontlightOff();
	void backlightOn();
	void backlightOff();

	bool isBLight() const{return bLight;}
	void setBLight(bool light){bLight = light;}
	bool isFLight() const{return fLight;}
	void setFLight(bool light){fLight = light;}

private:

	bool fLight;
	bool bLight;
};

#endif
