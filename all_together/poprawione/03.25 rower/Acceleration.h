#ifndef ACCELERATION_H_
#define ACCELERATION_H_

class Acceleration
{
public:

	Acceleration(int front = 1, int back = 1);
	void frontGearUp();
	void frontGearDown();
	void backGearUp();
	void backGearUp(int few);
	void backGearDown();
	void backGearDown(int few);

	int getBackGear() const{return backGear;}
	int getFrontGear() const{return frontGear;}
	void setBackGear(int backGear){this->backGear = backGear;}
	void setFrontGear(int frontGear){this->frontGear = frontGear;}

private:

	int frontGear;
	int backGear;
};

#endif
