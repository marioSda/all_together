#ifndef BIKE_H_
#define BIKE_H_

#include "Frame.h"
#include "Wheels.h"
#include "Lights.h"
#include "Acceleration.h"

class Bike
{

public:

	Bike();
	void accelerateOne();
	void accelerateFew(int few);
	void brakeOne();
	void brakeFew(int few);
	void stop();
	void turnOnLghts();
	void turnOffLghts();

	void changeFrame();
	void changeSizeWheels();
	void changeTires();

	Frame frame;
	Wheels wheels;
	Lights lights;
	Acceleration acceleration;

};

#endif


//////////////// bike contains: ////////////////
//* wheels *2	...........................class
//* frame		...........................class
//* shock
//* handlebar
//* seat
//* lights(front, back etc.)	...........class
//* brakes
//* acceleration system:	...............class
//		* gear front
//		* gear back
//
//=============	methods ===============
//* drive
//* brake
//* turn on/off the lights
//* change gear - 4 methods (2 - change one by one, 2 - change more than one)

