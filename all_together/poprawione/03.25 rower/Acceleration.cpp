#include "Acceleration.h"
#include <iostream>
using namespace std;

Acceleration::Acceleration(int front, int back)
{
	frontGear = front;
	backGear = back;
}

void Acceleration::frontGearUp()
{
	if (frontGear < 4)
		frontGear++;
	else
		cout << "Front gear is on max!" << endl;
}

void Acceleration::frontGearDown()
{
	if (frontGear > 1)
		frontGear--;
	else
		cout << "Front gear is on one!" << endl;
}

void Acceleration::backGearUp()
{
	if (backGear > 0 && backGear < 8)
	{
		if (backGear == 7 && frontGear < 4)
		{
			frontGear++;
			backGear = 0;
		}
		backGear++;
	}
}

void Acceleration::backGearUp(int few)
{
	int gears(backGear + few);

	if (gears > 21)
		cout << "Too many gears up! Above the limit!" << endl;
	else
	{
		while (gears >= 7)
		{
			frontGear++;
			gears -= 7;
		}
		backGear = gears;
	}
}

void Acceleration::backGearDown()
{
	if (backGear > 1)
		backGear--;
	else if (backGear == 1 && frontGear > 1)
	{
		frontGear--;
		backGear = 7;
	}
	else if (frontGear == 1 && backGear >= 1)
		backGear--;
}

void Acceleration::backGearDown(int few)
{
	int gears(backGear + (frontGear - 1) * 7);
	if (few >= gears)
		cout << "Too many gears down! Below the limit!" << endl;
	else
	{
		int tab[3][7] =	{
		{ 1, 2, 3, 4, 5, 6, 7 },
		{ 1, 2, 3, 4, 5, 6, 7 },
		{ 1, 2, 3, 4, 5, 6, 7 } };

		int *pGear = &tab[frontGear - 1][backGear - 1];
		pGear -= few;

		while (few >= backGear)
		{
			frontGear--;
			few -= 7;
		}
		backGear = *pGear;
	}
}
