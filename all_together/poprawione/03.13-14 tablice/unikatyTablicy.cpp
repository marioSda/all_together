#include <iostream>
///// Program zliczajacy unikalne wartosci w tablicy ///////
using namespace std;

int main()
{
    int powt(0);
    const int rozmiar = 7;
    int tab[rozmiar]= {1,1,1,4,5,6,7};

    for(int i=0; i<6; i++)
    {
        for(int j=i+1; j<7; j++)
        {
            if(tab[i]==tab[j])
                powt++;
        }
    }

    cout <<"W tablic wystepuja " << rozmiar-powt << " unikalne wartosci." << endl;

    return 0;
}
