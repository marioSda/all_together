#include <iostream>
using namespace std;
///// Program wypisujacy roznice dwoch kolejnych elementow tablicy ////

int main()
{
    const int max_size = 5;
    cout << "Podaj 5 liczb: " << endl;
    int t[max_size] = {};
    int temp;
    int roznica(0);

    for(int i=0; i<max_size; ++i)
    {
        cin >> temp;
        t[i] = temp;
    }

    for(int j=0; j<max_size-1; ++j)
    {
        roznica = t[j] - t[j + 1];
        cout << roznica << " ";
    }

    return 0;
}
