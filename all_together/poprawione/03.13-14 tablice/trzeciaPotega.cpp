#include <iostream>
#include <cmath>
using namespace std;

//////  Program wypisujacy 3 potege liczb w tablicy ///////

int main()
{
    const int max_size = 5;
    cout << "Podaj 5 liczb: " << endl;
    int t[max_size] = { };
    int temp;

    for (int i = 0; i < max_size; ++i)
    {
        cin >> temp;
        t[i] = temp;
    }

    for (int j = 0; j <= max_size; ++j)
        cout << pow(t[j],3) << " ";

    return 0;
}
