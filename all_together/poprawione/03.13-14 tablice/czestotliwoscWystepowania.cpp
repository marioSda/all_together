#include <iostream>
//// Program zliczajacy czestotliwosc wystepowania elementow w tablicy /////
using namespace std;

int main()
{
    int tab[5]= {2,2,2,4,5};
    int licznik=0;

        for(int i=0; i<5; ++i)
        {   licznik=0;
            for(int j=0; j<5; ++j)
            {
                if(tab[i]==tab[j])
                {
                    if(i==j)
                        break;
                    licznik++;
                }
            }
            cout << tab[i] << " wystapila... " << licznik+1 << " razy" << endl;
        }

    return 0;
}
