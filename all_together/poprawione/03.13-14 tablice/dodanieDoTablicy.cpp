#include <iostream>
using namespace std;
////// Program dodajacy element do tablicy na dana pozycje ////////

int main()
{
    int tab[100] = { 1, 2, 3, 4, 5, 6 };
    int pozycja;
    int liczba;
    int temp;

    for (int i=0; i<6; i++)
    {
        cout << tab[i] << " ";
    }
    cout << endl;
    cout << "Wprowadz indeks dla nowej wartosci: " << endl;
    cin >> pozycja;

    if (pozycja > 6)
        cout << "Pozycja wyracza poza zakres " << endl;
    else
        cout << "Wprowadz numer " << endl;
    cin >> liczba;

    for (int i=6; i>=pozycja; i--)
    {
        temp = tab[i];
        tab[i + 1] = temp;
    }

    tab[pozycja] = liczba;
    cout << "Nowa tablica" << endl;

    for (int i=0; i<6+1; i++)
    {
        cout << tab[i] << " ";
    }

    return 0;
}
