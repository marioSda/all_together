#include <iostream>
using namespace std;
/////// Program wypisujacy sume elementow tablicy //////////

int main()
{
    const int max_size = 5;
    cout << "Podaj 5 liczb: " << endl;
    int t[max_size] = {};
    int temp;
    int suma(0);

    for (int i=0; i<max_size; ++i)
    {
        cin >> temp;
        t[i] = temp;
    }

    for (int j=0; j<max_size; ++j)
    {
        suma+=t[j];
    }

    cout << "Suma wpisanych liczb do tablicy wynosi: " << suma << endl;

    return 0;
}
