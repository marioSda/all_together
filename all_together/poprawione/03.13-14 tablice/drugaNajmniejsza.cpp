#include <iostream>
using namespace std;
////// Program znajdujacy druga najmniejsza wartosc z tablicy liczb ///////
int main()
{
    const int max_size = 5;
    cout << "Podaj 5 liczb: " << endl;
    int t[max_size] = {};
    int temp;
    int min2 = 1000;
    int min = t[0]=1;

    for (int i=0; i<max_size; ++i)
    {
        cin >> temp;
        t[i] = temp;

        if (t[i] < min)
        {
            min2 = min;
            min = t[i];
        }
        if (t[i] > min && t[i] < min2)
            min2 = t[i];
    }

    cout << "Druga najmniejsza liczba to: " << min2 << endl;
    cout << "Najmniejsza liczba to: " << min << endl;

    return 0;
}
