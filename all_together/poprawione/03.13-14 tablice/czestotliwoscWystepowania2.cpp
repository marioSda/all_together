#include <iostream>
using namespace std;
///////////// Program zliczaj¹cy duplikaty w  tablicy i wypisujacy czestotliwosc////////////////
int main()
{
    int tab[1000] = {};
    int n;
    int temp(0);
    int licznik(0);
    cout << "Wprowadz rozmiar tablicy : ";
    cin >> n;
    cout << "Wprowadz " << n << " elementow tablicy " << endl;

    for (int i = 0; i < n; i++)
    {
        cin >> tab[i];
    }

    cout << "Powtarzajace sie elementy w tablicy: ";

    for (int i = 0; i < n-1; i++)
    {
        temp = tab[i];
        for (int j = i + 1; j < n-1; j++)
        {
            if (tab[j] == temp)
            {
                cout << tab[j] << " ";
                licznik++;
            }
            break;
        }
    }

    cout << ", " <<licznik<< " razy.";

    return 0;
}

