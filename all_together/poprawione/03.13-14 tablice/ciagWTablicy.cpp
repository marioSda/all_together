#include <iostream>
using namespace std;
///// Program generujacy wartosci w tablicy ciagiem /////
int main()
{
    int wielkosc;
    cout<<"Podaj wielkosc tablicy: ";
    cin >> wielkosc;
    const int rozmiar = wielkosc;
    int tab[rozmiar] = {};
    tab[0] = 2;

    for(int i = 1; i < rozmiar; i++)
        tab[i]=tab[i-1]+2;

    for(int i = 0; i < rozmiar; i++)
        cout << tab[i]<< " ";

    return 0;
}
