#include <iostream>
using namespace std;

int main()
{
    int v1, v2, liczba;

    cout << "Podaj wspolrzedne wektora: ";
    cin >> v1 >> v2;
    cout << "Podaj liczbe przez jaka chcesz pomnozyc wektor: ";
    cin >> liczba;

    int vector1[2] = {v1, v2};

    cout << "Wspolrzedne wektora: ";
    for (int i = 0; i < 2; i++)
        cout << vector1[i] << " ";

    cout << endl;

    int iloczyn[2] = {};

    for (int i = 0; i < 2; i++)
        iloczyn[i] = vector1[i] * liczba;

    cout << "Wspolrzedne wektora bedacego iloczynem "<< liczba << ", wynosza: ";

    for (int j = 0; j < 2; j++)
        cout << iloczyn[j] << " ";

    cout<<endl;

    return 0;
}
