#include <iostream>
using namespace std;
//////Program wczytujacy podana ilosc liczb i wypisujacy je na ekran w odwrotnej kolejnosci/////////
int main()
{
    const int max_size = 10;
    cout << "Podaj 10 liczb: " << endl;
    int t[max_size] = {};
    int temp;

    for (int i=0; i<max_size; ++i)
    {
        cin >> temp;
        t[i] = temp;
    }

    for (int j=max_size-1; j>=0; --j)
    {
        cout << t[j] << " ";
    }

    return 0;
}
