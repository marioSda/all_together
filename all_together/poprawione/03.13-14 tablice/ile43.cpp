#include <iostream>
using namespace std;
//////// Program zliczajacy ilosc wystapien liczby 43 w tablicy liczb /////////
int main()
{
    const int max_size = 5;
    cout << "Podaj 5 liczb: " << endl;
    int t[max_size] = {};
    int temp;
    int licznik = 0;

    for (int i = 0; i < max_size; ++i)
    {
        cin >> temp;
        t[i] = temp;
        if(temp == 43)
            licznik++;

    }

    cout <<"Liczba 43 padla: "<<licznik<<" razy."<<endl;

    return 0;
}
