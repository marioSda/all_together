#include <iostream>
using namespace std;

int main()
{
///// Program szukajacy elementu w tablicy. Zwraca indeks ////

    const int max_size = 5;

    int t[max_size] = { 1, 2, 3, 4, 5 };
    int liczba;
    cout << "jakiej liczby szukasz? ";
    cin >> liczba;

    for (int i = 0; i < max_size; ++i)
    {
        if (t[i] == liczba)
        {
            cout << "Indeks szukanej liczby: " << i << endl;
        }
    }

    return 0;
}
