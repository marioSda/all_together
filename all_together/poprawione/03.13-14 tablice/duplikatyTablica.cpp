#include <iostream>
//// Program zliczajacy duplikaty w tablicy /////
using namespace std;

int main()
{
    int tab[5]= {1,2,3,3,3};
    int licznik(0);
    for(int i=0; i<5; i++)
    {
        for(int j=i+1; j<5; j++)
        {
            if(tab[j]==tab[i]&&i!=j)
                licznik++;
            break;
        }
    }

    cout << "Elementow powtarzajacych sie jest: " << licznik+1 << endl;

    return 0;
}
