#include <iostream>
using namespace std;
/////// Program kopiujacy wartosci miedzy tablicami ///////

int main()
{
    const int max_size = 5;
    cout << "Podaj 5 liczb dla tablicy nr 1: " << endl;
    int t[max_size] = { };
    int t2[max_size] = { };
    int temp;

    for (int i=0; i<max_size; ++i)
    {
        cin >> temp;
        t[i] = temp;
    }

    for (int i=0; i<max_size; ++i)
    {
        t2[i] = t[i];
    }

    cout << "Elementy tablicy nr 1 po skopiowaniu: " << endl;

    for (int i=0; i<max_size; ++i)
    {
        cout << t[i] << " ";
    }

    cout<<endl;
    cout << "Elementy tablicy nr 2 po skopiowaniu: " << endl;

    for (int i=0; i<max_size; ++i)
    {
        cout << t2[i] << " ";
    }

    return 0;
}
