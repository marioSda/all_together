#include <iostream>
using namespace std;

bool czyPrzestepny(int r);

int main()
{
    int dzien(0), miesiac(0), rok(0);

    while(true)
    {
        cout<<"Podaj date w podanym formacie DD/MM/YYYY "<<endl;
        cin>>dzien>>miesiac>>rok;

        if(miesiac==1||miesiac==3||miesiac==5||miesiac==7||miesiac==8||miesiac==10||miesiac==12)
        {
            if(dzien<1||dzien>31)
                cout<<"Bledna liczba dni!"<<endl;

            if(rok==0)
                cout<<"Zly rok! "<< endl;

            if(czyPrzestepny(rok))
                cout<<"(rok przestepny)"<<endl;
            else
                cout<<"(rok nie jest przestepny)"<<endl;
        }
        else if(miesiac==2)
        {
            if(czyPrzestepny(rok) == false && (dzien < 1 || dzien >= 29))
                cout<<"Bledna liczba dni!"<<endl;
            else if(czyPrzestepny(rok) && (dzien < 1 || dzien > 29))
                cout<<"Bledna liczba dni!"<<endl;

            if(rok==0)
                cout<<"Zly rok! "<< endl;

                if(czyPrzestepny(rok))
                    cout<<"(rok przestepny)"<<endl;
                else
                    cout<<"(rok nie jest przestepny)"<<endl;
        }
        else if(miesiac==4||miesiac==6||miesiac==9||miesiac==11)
        {
            if(dzien<1||dzien>=30)
                cout<<"Bledna liczba dni!"<<endl;
            if(rok==0)
                cout<<"Zly rok! "<< endl;

            if(czyPrzestepny(rok))
                cout<<"(rok przestepny)"<<endl;
            else
                cout<<"(rok nie jest przestepny)"<<endl;
        }
        else
            cout<<"Zly miesiac! "<< endl;

        cout<<dzien<<"/"<<miesiac<<"/"<<rok<<endl;
    }
    return 0;
}

bool czyPrzestepny(int r)
{
    if ((r % 4) == 0 && (r % 100) != 0 && r != 0 || (r % 400) == 0)
        return 1;
    else
        return 0;
}
