#include <iostream>
using namespace std;

int main()
{
    double wartosc(0);
    int wybor(0);
    double farenheit(0);
    double celcjusz(0);

    while(true)
    {
        cout<<"Wprowadz wartosc ";
        cin>>wartosc;
        cout<<"... w stopniach celcjusza(1) czy fahrenheitach(2) ? \n";
        cin>>wybor;

        if(wybor>2||wybor<0)
            cout<<"Zle! Wybierz 1 dla celcjusza lub 2 dla fahrenheitow! \n";
        else
        {
            switch(wybor)
            {
            case 1:
            {
                farenheit = 1.8*wartosc+32;
                cout<<wartosc<<" stopni celcjusza ---------> "<<farenheit<<" fahrenheitow "<<endl;
                break;
            }
            case 2:
            {
                celcjusz = (wartosc-32)/1.8;
                cout<<wartosc<<" fahrenheitow ---------> "<<celcjusz<<" stopni celcjusza "<<endl;
                break;
            }
            }
        }
    }

    return 0;
}
