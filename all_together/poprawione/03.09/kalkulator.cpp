#include <iostream>
using namespace std;

int main()
{
    int a, b, operacja;

    while (true)
    {
        cout << "*******************************************" << endl;
        cout << "Podaj dwie liczby \n"; cin >> a >> b;
        cout << "Jaka operacje chcesz przeprowadzic? \n";
        cout << "1. dodawanie \n";
        cout << "2. odejmowanie \n";
        cout << "3. mnozenie \n";
        cout << "4. dzielenie \n";
        cout << "5. zakoncz \n";
        cin >> operacja;

        if (operacja == 1)
            cout << a << " + " << b << " = " << a + b << endl;
        else if (operacja == 2)
            cout << a << " - " << b << " = " << a - b << endl;
        else if (operacja == 3)
            cout << a << " * " << b << " = " << a * b << endl;
        else if (operacja == 4)
            cout << a << " / " << b << " = " << a / b << endl;
        else if (operacja == 5)
        {
            cout << "Koniec programu " << endl;
            break;
        }
        else
            cout << "Prosze wybrac opcje od 1 do 5 " << endl;
    }
    return 0;
}
