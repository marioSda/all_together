#include <iostream>
using namespace std;

bool czyPrzestepny(int x);

int main()
{
    while(true)
    {
        cout<<endl;
        cout << "Wprowadz date w takim formacie DD/MM/RRRR " << endl;
        int dzien(0), miesiac(0), rok(0);
        cin >> dzien >> miesiac >> rok;

        switch(miesiac)
        {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
        {
            if(dzien<1||dzien>31)
                cout<<"Bledna liczba dni!"<<endl;

            if(rok==0)
                cout<<"Zly rok! "<< endl;

            if(czyPrzestepny(rok))
                cout<<"(rok przestepny)"<<endl;
            else
                cout<<"(rok nie jest przestepny)"<<endl;

            break;
        }
        case 2:
        {
            if(czyPrzestepny(rok) == false && (dzien < 1 || dzien >= 29))
                cout<<"Bledna liczba dni!"<<endl;
            else if(czyPrzestepny(rok) && (dzien < 1 || dzien > 29))
                cout<<"Bledna liczba dni!"<<endl;

            if(rok==0)
                cout<<"Zly rok! "<< endl;

            if(czyPrzestepny(rok))
                cout<<"(rok przestepny)"<<endl;
            else
                cout<<"(rok nie jest przestepny)"<<endl;

            break;
        }
        case 4:
        case 6:
        case 9:
        case 11:
        {
            if(dzien<1||dzien>=30)
                cout<<"Bledna liczba dni!"<<endl;

            if(rok==0)
                cout<<"Zly rok! "<< endl;

            if(czyPrzestepny(rok))
                cout<<"(rok przestepny)"<<endl;
            else
                cout<<"(rok nie jest przestepny)"<<endl;

            break;
        }
        default:
            cout<<"Zly miesiac! "<< endl;
            break;
        }
        cout<<dzien<<"/"<<miesiac<<"/"<<rok<<endl;
    }
    return 0;
}

bool czyPrzestepny(int r)
{
    if ((r % 4) == 0 && (r % 100) != 0 && r != 0 || (r % 400) == 0)
        return 1;
    else
        return 0;
}
