#include <iostream>
using namespace std;

int main()
{
    // Rownoczesna iteracja w jednej petli

    for(int i=1, j=20; i<=20; ++i, --j)
    {
        cout << i << " " << j << endl;
    }

    cout << endl;
    cout << "*************************************\n";

    // Potegowanie poprzez przesuniecie bitowe o jeden w lewo

    int x = 2;

    for(int i=1; i<20; i++)
    {
        cout << "potega: " << i << " : " << x << endl;
        x = x << 1;
    }
    cout << endl;
    cout << "*************************************\n";

    // Dzielenie bez reszty przez dwa poprzez przesuniecie bitowe w prawo

    int y = 10000000;

    for(int i=0; i<20; i++)
    {
        cout << "dzielenie przez dwa bez reszty: " << y << endl;
        y = y >> 1;
    }

    return 0;
}
