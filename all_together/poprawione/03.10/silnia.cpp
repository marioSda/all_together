#include <iostream>
using namespace std;

int silnia(int liczba);

int main()
{
	int liczba;
	cout << "Wprowadz liczbe ";
	cin >> liczba;
	cout << "Silnia z " << liczba << " wynosi " << (long long)silnia(liczba) << endl;

	return 0;
}

int silnia(int liczba)
{
	if (liczba == 0)
		return 1;
	else
		return liczba * silnia(liczba - 1);
}
