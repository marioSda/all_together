#include <cmath>
#include <iostream>
using namespace std;

int main()
{
    bool flaga(true);
    int wybor;

    while(flaga)
    {
        cout<<"**** pola powierzchni figur ****\n ";
        cout<<" 1. kwadrat\n ";
        cout<<" 2. prostokat\n ";
        cout<<" 3. trojkat\n ";
        cout<<" 4. kolo\n ";
        cout<<" 5. szescian\n ";
        cout<<" 6. prostopadloscian\n ";
        cout<<" 7. kula\n ";
        cout<<" 8. ostroslup\n ";
        cout<<" 9. zakoncz\n ";
        cout<<endl;
        cout<<"Prosze podac wybor: ";
        cin>>wybor;

        if(wybor<=0||wybor>9)
            cout<<"Prosze wybrac z zakresu 1-9 "<<endl;
        else
        {
            switch(wybor)
            {
            case 1:
                float bok;
                cout<<"Prosze podac dlugosc boku kwadratu: ";
                cin>>bok;
                cout<<endl;
                cout<<"Pole kwadratu o boku " << bok << " wynosi: " << (float)bok*bok<<endl;
                cout<<endl;
                break;
            case 2:
                float bok1;
                float bok2;
                cout<<"Prosze podac dlugosc bokow prostokata: ";
                cin>>bok1>>bok2;
                cout<<endl;
                cout<<"Pole prostokata o bokach " << bok1 <<", " << bok2 << " wynosi: " << (float)bok1*bok2<<endl;
                cout<<endl;
                break;
            case 3:
                float podstawa;
                float wysokosc;
                cout<<"Prosze podac dlugosc podstawy trojkata oraz jego wysokosc: ";
                cin>>podstawa>>wysokosc;
                cout<<endl;
                cout<<"Pole trojkata o podstawie: " << podstawa << " i wysokosci: " << wysokosc <<
                    " wynosi " << (float)0.5*podstawa*wysokosc << endl;
                cout<<endl;
                break;
            case 4:
                float promien;
                cout<<"Prosze podac dlugosc promienia kola: ";
                cin>>promien;
                cout<<endl;
                cout<<"Pole powierzchni kola o promieniu: " << promien << " wynosi: "<< (float)M_PI*promien*promien << endl;
                cout<<endl;
                break;
            case 5:
                float krawedz;
                cout<<"Prosze podac dlugosc krawedzi szescianu: ";
                cin>>krawedz;
                cout<<endl;
                cout<<"Pole powierzchni szescianu o krawedzi: " << krawedz << " wynosi: "<< (float)6*krawedz*krawedz << endl;
                cout<<endl;
                break;
            case 6:
                float a;
                float b;
                float c;
                cout<<"Prosze podac dlugosci krawedzi prostopadloscianu: ";
                cin>>a>>b>>c;
                cout<<endl;
                cout<<"Pole powierzchni prostopadloscianu o krawedziach: " << a << ", " << b << ", " <<  c << " wynosi: "<<
                    (float)2*(a*b+a*c+b*c) << endl;
                cout<<endl;
                break;
            case 7:
                float promienK;
                cout<<"Prosze podac dlugosc promienia kuli: ";
                cin>>promienK;
                cout<<endl;
                cout<<"Pole powierzchni kuli o promieniu: " << promienK << " wynosi: "<< (float)M_PI*4*promienK*promienK << endl;
                cout<<endl;
                break;
            case 8:
                cout<<"Jakiego rodzaju ostroslup cie interesuje?\n";
                // cout<<" 1. trojkatny \n";
                cout<<" 1. trojkatny (prawidlowy) \n";
                cout<<" 2. czworokatny \n";
                cin>>wybor;

                switch(wybor)
                {
                    break;
                case 1:
                    float a2;
                    float h2;
                    cout<<"Prosze podac dlugosc krawedzi podstawy ostroslupa: ";
                    cin>>a2;
                    cout<<"Prosze podac wysokosc sciany bocznej ostroslupa: ";
                    cin>>h2;
                    cout<<endl;
                    cout<<"Pole powierzchni ostroslupa prawidlowego o krawedzi podstawy: " << a2 << " i wysokosci sciany bocznej rownej: " <<
                        h2 << " wynosi: "<< (float)(a2*a2)*sqrt(3)/4+3*(a2*h2)*0.5 << endl;
                    cout<<endl;
                    break;
                case 2:
                    int a3;
                    int h3;
                    cout<<"Prosze podac dlugosc krawedzi podstawy ostroslupa: ";
                    cin>>a3;
                    cout<<"Prosze podac wysokosc sciany bocznej ostroslupa: ";
                    cin>>h3;
                    cout<<endl;
                    cout<<"Pole powierzchni ostroslupa o krawedzi podstawy: " << a3 << " i wysokosci sciany bocznej rownej: " <<
                        h3 << " wynosi: "<< (float)(a3*a3)+(2*a3*h3) << endl;
                    cout<<endl;
                    break;
                }
                break;
            case 9:
                cout<<endl;
                cout<<"Zamykam... ";
                flaga = false;
                break;
            }
        }
    }

    return 0;
}

