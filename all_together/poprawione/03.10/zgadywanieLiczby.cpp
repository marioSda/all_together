#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

int main()
{
    srand (time(NULL));
    int random = rand() % 20 + 1;
    int liczba;
    int iloscZliczen=0;

    cout<<"Zgadnij liczbe od 1 do 20: "<<endl;
    do
    {
        cin>>liczba;

        if(liczba<random)
            cout<<"Liczba jest wyzej! \n";
        else
            cout<<"Liczba jest nizej! \n";
        iloscZliczen++;
    }
    while(random!=liczba);

    cout<<"Brawo! Udalo sie za "<<iloscZliczen<<" razem... Ukryta liczba to: "<<random<<endl;

    return 0;
}
