
#include "Base.h"
#include <iostream>

Base::Base(){}

void Base::f()
{
	std::cout <<"metoda f z klasy Base " << std::endl;
}

void Base::g(int x)
{
	std::cout << "metoda g z parametrem: " << x << std::endl;
}
