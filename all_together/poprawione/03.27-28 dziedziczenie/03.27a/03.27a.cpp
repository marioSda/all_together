#include <iostream>
#include "Animal.h"
#include "Dog.h"
#include "Cat.h"
#include "Fish.h"
#include "Base.h"
#include "Derived.h"

int main()
{

//	Animal animal;
//	Dog dog;
//	Cat cat;
//	Fish fish;
//
//	animal.speak();
//	dog.speak();
//	cat.speak();
//	cat.Animal::speak();			/// Odwolywanie sie do klasy Animal z poziomu klasy Cat;
//
//	Animal *someAnimal = &dog;
//	someAnimal->speak();
//
//	std::cout << std::endl;
//
//	Animal* tab[3] = { &dog, &cat, &fish };
//
//	for (int i = 0; i < 3; i++)
//	{
//		tab[i]->speak();
//	}

	Base base;
	Derived derived;

	base.f();
	derived.f();

	Base *pBase = &base;
	pBase->f();
	derived.g(4);

	return 0;
}
