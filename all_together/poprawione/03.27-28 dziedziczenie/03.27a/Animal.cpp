#include "Animal.h"
#include "Cat.h"
#include "Dog.h"
#include "Fish.h"
#include <iostream>

Animal::Animal() {}

void Animal::speak() {
	std::cout << "Animal makes sound" << std::endl;
}
