#ifndef FISH_H_
#define FISH_H_
#include <iostream>
#include "Animal.h"

class Fish: public Animal
{
public:

	Fish();
	void speak();
};

#endif
