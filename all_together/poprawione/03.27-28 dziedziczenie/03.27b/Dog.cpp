#include "Dog.h"
#include <iostream>
Dog::Dog()
{
	x = 2;
	y = 2;
}

void Dog::speak()
{
	std::cout << "Hau Hau" << std::endl;
}

bool Dog::operator==(const Dog& other) const
{
	if (x == other.x && y == other.y)
		return true;
	return false;
}
bool Dog::operator!=(const Dog& other) const
{
	return !operator==(other);
}
bool Dog::operator>(const Dog& other) const
{
	if (x > other.x && y > other.y)
		return true;
	return false;
}
bool Dog::operator<(const Dog& other) const
{
	if (x < other.x && y < other.y)
		return true;
	return false;
}
