#ifndef DOG_H_
#define DOG_H_

#include "Animal.h"

class Dog: public Animal {
public:
	using Animal::speak;
	Dog();
	void speak();
	bool operator==(const Dog& other)const;
	bool operator!=(const Dog& other)const;
	bool operator>(const Dog& other)const;
	bool operator<(const Dog& other)const;
private:
	int x;
	int y;
};

#endif
