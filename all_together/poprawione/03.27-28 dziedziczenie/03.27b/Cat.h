#ifndef CAT_H_
#define CAT_H_

#include "Animal.h"

class Cat: public Animal {
public:
	using Animal::speak;
	Cat();
	void speak();
	bool operator==(const Cat& other)const;
	bool operator!=(const Cat& other)const;
	bool operator>(const Cat& other)const;
	bool operator<(const Cat& other)const;

private:
	int x;
	int y;

};

#endif

