#include <iostream>
#include "Cat.h"

Cat::Cat()
{
	x = 3;
	y = 3;
}

void Cat::speak()
{
	std::cout << "Miau Miau" << std::endl;
}
bool Cat::operator==(const Cat& other) const
{
	if (x == other.x && y == other.y)
		return true;
	return false;
}
bool Cat::operator!=(const Cat& other) const
{
	return !operator==(other);
}
bool Cat::operator>(const Cat& other) const
{
	if (x > other.x && y > other.y)
		return true;
	return false;
}
bool Cat::operator<(const Cat& other) const
{
	if (x < other.x && y < other.y)
		return true;
	return false;
}

