#include "Animal.h"
#include <iostream>
Animal::Animal()
{
	x = 3;
	y = 2;
}

void Animal::speak(int i)
{
	std::cout << "Animal makes sound!" << i << std::endl;
}

bool Animal::operator==(const Animal& other) const
{
	if(x == other.x && y == other.y)
		return true;
	return false;
}

bool Animal::operator!=(const Animal& other) const
{
	return !operator==(other);
}

bool Animal::operator>(const Animal& other) const
{
	if(x > other.x)
	{
		if(y > other.y)
			return true;
	}
	return false;
}
