#ifndef ANIMAL_H_
#define ANIMAL_H_

class Animal {
public:
	Animal();
	virtual void speak(int i);
	bool operator==(const Animal& other)const;
	bool operator!=(const Animal& other)const;
	bool operator>(const Animal& other)const;

	int x;
	int y;
};

#endif
