#include <iostream>
#include "Animal.h"
#include "Dog.h"
#include "Cat.h"

using namespace std;

int main() {

	Animal animal;
	Animal animal2;
	Dog dog;
	Dog dog1;
	Cat cat;
	Cat cat1;

	cout << animal.operator ==(animal2) << endl;
	cout << "Different than? " << (animal != animal2) << endl;
	cout << "Bigger than? " << (animal > animal2) << endl;

	cout << "Equal to each other? " << dog.operator==(dog1) << endl;
	cout << "Different from each other? " << dog.operator !=(dog1) << endl;
	cout << "Bigger than? " << (dog>dog1) << endl;
	cout << "Smaller than? " << (dog<dog1) << endl;

	cout << "Equal to each other? " << cat.operator ==(cat1) << endl;
	cout << "Different from each other? " << cat.operator!=(cat1) << endl;
	cout << "Bigger than ? " << cat.operator>(cat1) << endl;
	cout << "Smaller than ? " << cat.operator<(cat1) << endl;

	return 0;
}
