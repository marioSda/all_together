#ifndef ANIMAL_H
#define ANIMAL_H
#include <string>

class Animal
{
public:
    Animal(std::string n);
    std::string giveName(){return name;};
    void speak();

private:
    virtual void giveSound()=0;
    std::string name;

};

#endif
