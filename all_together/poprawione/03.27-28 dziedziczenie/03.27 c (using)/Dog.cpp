#include "Dog.h"
#include <string>
#include <iostream>

Dog::Dog(std::string n):
    Animal(n)
{

}

void Dog::giveSound()
{
    std::cout << "Woff woff" << std::endl;
}
