#include <iostream>
#include "Animal.h"
#include "Dog.h"
#include "Cat.h"
#include "Base.h"
#include "Derived.h"

using namespace std;

int main()
{
    Cat cat("burek");
    cat.speak();
    Dog dog("kajtek");
    dog.speak();            // kot i pies wywoluja metode speak ktora jest zdefiniowana w klasie bazowej Animal
                            // w speak() jest odwolanie do giveSound() ktore w klasach dziedziczacych jest
                            // zdefiniowane i oznaczone jako prywatne.
    cout<<endl;
    Base base;
    base.f();
    base.g(2);
    Derived derived;
    derived.f(3);
    derived.g();

    derived.f();            // dzieki using mamy dostep do nadpisanych metod z poziomu obiektu klasy dziedziczacej.
    derived.g(45);

    return 0;
}
