#include "Animal.h"
#include <string>
#include <iostream>

Animal::Animal(std::string n)
{
    name = n;
}

void Animal::speak()
{
    std::cout << "Animal named " << name << " makes sound. " << std::endl;
    giveSound();
}




