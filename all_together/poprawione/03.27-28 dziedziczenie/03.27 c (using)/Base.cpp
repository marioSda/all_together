#include "Base.h"
#include <iostream>

Base::Base()
{

}

void Base::f()
{
    std::cout << "Function f from class Base " << std::endl;
}

void Base::g(int x)
{
    std::cout << "Function g from class Base with an argument: " << x << std::endl;
}
