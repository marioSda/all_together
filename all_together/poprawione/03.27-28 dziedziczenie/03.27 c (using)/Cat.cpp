#include "Cat.h"
#include <iostream>
#include <string>

Cat::Cat(std::string n):
    Animal(n)
{

}

void Cat::giveSound()
{
    std::cout << "Meaow meaow " << std::endl;
}
