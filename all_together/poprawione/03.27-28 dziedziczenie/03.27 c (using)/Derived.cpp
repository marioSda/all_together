#include "Derived.h"
#include <iostream>

Derived::Derived()
{

}

void Derived::f(int x)
{
    std::cout << "Function f from class Derived with an argument: " << x << std::endl;
}

void Derived::g()
{
    std::cout << "Function g from class Derived " << std::endl;
}
