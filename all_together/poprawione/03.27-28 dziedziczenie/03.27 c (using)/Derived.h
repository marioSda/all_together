#ifndef DERIVED_H
#define DERIVED_H

#include "Base.h"


class Derived : public Base
{
public:
    using Base::f;      // dzieki using mamy dostep do oryginalnych funkcji klasy bazowej
    using Base::g;
    Derived();
    void f(int x);
    void g();

};

#endif
