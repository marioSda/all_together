#include <iostream>
using namespace std;
/// iloczyn wektorow z wykorzystaniem wskaznikow ///
void iloczyn(int *p1, int *p2, int *p3, int n);

int main()
{
    int tab1[3]= {2,3,4};
    int tab2[3]= {5,6,7};
    int tab3[3]= {};
    cout << "Dane wektory: \n"
         "tab1 = ["<<tab1[0]<<","<<
         tab1[1]<<","<<tab1[2]<<"] \n";
    cout << "tab2 = ["<<tab2[0]<<","<<
         tab2[1]<<","<<tab2[2]<<"] \n";

    int *pTab1=tab1;
    int *pTab2=tab2;
    int *pTab3=tab3;
    iloczyn(pTab1, pTab2, pTab3, 3);
    pTab3=tab3;

    cout<< "Iloczyn wektorow: "<<endl;
    cout<< "tab3 = [";
    for(int i = 0; i < 3; i++, pTab3++)
        cout <<*pTab3<<",";

    cout<<"]";
    return 0;
}

void iloczyn(int *p1, int *p2, int *p3, int n)
{
    for(int i = 0; i < n; i++,p3++,p2++,p1++)
        *p3=*p1*(*p2);
}
