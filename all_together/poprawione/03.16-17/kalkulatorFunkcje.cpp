#include <iostream>
using namespace std;
/// kalkulator - funkcje ////

double dodawanie(double *a, double *b);
double odejmowanie(double *a, double *b);
double mnozenie(double *a, double *b);
double dzielenie(double *a, double *b);

int main()
{
    while(true)
    {
        double a, b, c;
        cout <<"Podaj dwie liczby: ";
        cin >> a >> b ;
        cout <<"Jakie dzialanie chcesz przeprowadzic? \n"
             "1. dodawanie\n"
             "2. odejmowanie\n"
             "3. mnozenie\n"
             "4. dzielenie\n"
             "5. zakoncz " << endl;
        cin >> c;
        if(c==1)cout<<"wynik: "<<dodawanie(&a,&b)<<endl;
        if(c==2)cout<<"wynik: "<<odejmowanie(&a,&b)<<endl;
        if(c==3)cout<<"wynik: "<<mnozenie(&a,&b)<<endl;
        if(c==4)cout<<"wynik: "<<dzielenie(&a,&b)<<endl;
        if(c==5)
        {
            cout<<" koniec programu "<<endl;
            break;
        }
        if(c>5||c<1)
        {
            cout<<"Wprowadz z przedzialu 1-4 \n";
            break;
        }
    }
    return 0;
}
double dodawanie(double *a, double *b){return *a+*b;}
double odejmowanie(double *a, double *b){return *a-*b;}
double mnozenie(double *a, double *b){return *a*(*b);}
double dzielenie(double *a, double *b)
{
    if(*b!=0)return *a/(*b);
    else return 0;
}
