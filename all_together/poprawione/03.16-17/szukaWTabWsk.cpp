#include <iostream>
using namespace std;

int main()
{
///// Program szukajacy elementu w tablicy. Zwraca indeks ////
    //// Wskaznik ////
    const int max_size = 5;
    int t[max_size] = { 1, 2, 3, 4, 5 };
    int pozycja;
    cout << "jakiej liczby szukasz? ";
    cin >> pozycja;
    int *wskT = t;

    for (int i = 0; i < max_size; ++i,++wskT)
    {
        if (*wskT == pozycja)
            cout << "Pozycja szukanej liczby to: " << i + 1 << endl;
    }

    return 0;
}
