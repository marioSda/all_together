#include <iostream>
#include <cmath>
using namespace std;

//////  Program wypisujacy 3 potege liczb w tablicy ///////
            //// Wskaznik ////
int main()
{
	const int max_size = 5;
	cout << "Podaj liczby: " << endl;
	int t[max_size] = {};
	int *wskT = t;

	for (int i=0; i<max_size; ++i,++wskT)
		cin >> *wskT;

	wskT=t;     // ustawiamy wskaznik z powrotem na pierwszy element tablicy

	for (int j=0; j<max_size; ++j, wskT++)
		cout << pow(*wskT,3) << " ";

	return 0;
}
