#include <iostream>
using namespace std;

/// Fibonacci sequence ///

int fib(int n)
{
	if(n==0)
		return 0;
	if(n==1)
		return 1;
	else
		return fib(n-2)+fib(n-1);
}

int main() {

	int x;
	cout << "Type a number of an element in Fibonacci's sequence: ";
	cin >> x;

	cout <<"Element number: " << x << " in Fibonacci's sequence is: "<< fib(x) << endl;

	return 0;
}
