#include <iostream>
using namespace std;

//// Sine ////

int silnia(int n)
{
	if (n >= 0)
	{
		if (n == 0)
			return 1;
		else
			return n * silnia(n - 1);

	}
	else
	{
		if (n == 0)
			return 1;
		else
			return n * silnia(n + 1);
	}
}

int main()
{

	int numer = 3;
	cout << "Type a number: ";
	cin >> numer;
	cout << silnia(numer) << flush;
	cout << " <--- That is a sine of the given number." << endl;

	return 0;
}
