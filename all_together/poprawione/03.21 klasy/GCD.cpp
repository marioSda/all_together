#include <iostream>
using namespace std;

/// GCD - Greatest Common Divisor ///

int nwd(int k, int n)
{
	if(k==0)
		return n;
	else
		return nwd(n%k,k);
}

int main() {

	int k;
	int n;
	cout << "Type two numbers: ";
	cin >> k >> n;

	cout <<"GCD is equal to: " << nwd(k,n) << " ";

	return 0;
}
