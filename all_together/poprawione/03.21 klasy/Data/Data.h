#ifndef DATA_H
#define DATA_H

class Data
{
public:
    Data(int r, int m, int d);
    int getRok();
    int getMiesiac();
    int getDzien();

private:
    int rok;
    int miesiac;
    int dzien;
};

#endif // DATA_H
