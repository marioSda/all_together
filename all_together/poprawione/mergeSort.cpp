#include <iostream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <time.h>

using namespace std;

const int N = 20;
int d[N], p[N];

// Procedura sortujaca

void MergeSort(int i_p, int i_k)
{
    int i_s,i1,i2,i;

    i_s = (i_p + i_k + 1)/2;    // np(0+7+1)/2 --> i_s = 4 indeks srodkowego elementu
    if(i_s - i_p > 1) MergeSort(i_p, i_s-1);
    if(i_k - i_s > 0) MergeSort(i_s, i_k);
    i1 = i_p; i2 = i_s;
    for(i = i_p; i <= i_k; i++)
        p[i] = ((i1 == i_s) || ((i2 <= i_k) && (d[i1] > d[i2]))) ? d[i2++] : d[i1++];
    for(i = i_p; i <= i_k; i++) d[i] = p[i];

}

// Program glowny

int main()
{
   int i;

    srand((int)time(NULL));
    for(i = 0; i < N; i++)d[i] = rand()% 100;
    for(i = 0; i < N; i++)cout << " " << d[i];
    cout << endl;

    MergeSort(0,N-1);

    cout << "Po sortowaniu " << endl;

    for(i = 0; i < N; i++) cout << " " << d[i];
    cout << endl;

    return 0;
}
