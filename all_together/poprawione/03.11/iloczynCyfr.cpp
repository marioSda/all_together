#include <iostream>
using namespace std;

int main()
{
    int liczba;
    cout << "Podaj liczbe: ";
    cin >> liczba;
    int cyfra;
    int iloczyn(1);

    while(liczba > 0)
    {
        cyfra = liczba % 10;
        liczba /= 10;
        iloczyn *= cyfra;
    }

    cout << "Iloczyn cyfr podanej liczby wynosi: " << iloczyn << endl;

    return 0;
}
