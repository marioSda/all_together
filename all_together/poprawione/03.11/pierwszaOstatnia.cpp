#include <iostream>
using namespace std;

int main()
{
	int liczba;
	cout << "Podaj liczbe: ";
	cin >> liczba;
	int cyfra = liczba % 10;

	while (liczba > 10)
	{
		liczba /= 10;
	}

	cout << "Pierwsza cyfra w liczbie jest: " << liczba << endl;
	cout << "Ostatnia cyfra w liczbie jest: " << cyfra << endl;

	return 0;
}
