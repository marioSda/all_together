#include <iostream>
using namespace std;

int main()
{
///////////////////////////////// 1 //////////////////////////////////
    int liczba;
    cout<<"Wprowadz liczbe: ";
    cin>>liczba;

    switch(liczba)
    {
    default:
        if(liczba%2==0)
            cout<<"Liczba "<<liczba<<" jest parzysta."<<endl;
        else
            cout<<"Liczba "<<liczba<<" nie jest parzysta."<<endl;
        break;
    }

///////////////////////////////// 2 //////////////////////////////////

    int liczba2;
    int wynik;
    cout<<"Wprowadz liczbe: ";
    cin>>liczba2;

    if(liczba2%2==0)wynik=1;
    else wynik=2;

    switch(wynik)
    {
    case 1:
        cout<<"Liczba " << liczba2 <<" jest parzysta. "<<endl;
        break;
    case 2:
        cout<<"Liczba " << liczba2 <<" nie jest parzysta. "<<endl;
        break;
    }

    return 0;
}
