#include <iostream>
using namespace std;

int main() {

	int miesiac;
	cout<<"Podaj numer miesiaca: "<<endl;
	cin>>miesiac;
	if(miesiac==1||miesiac==3||miesiac==5||miesiac==7||miesiac==8||miesiac==10||miesiac==12)
		cout<<"Ilosc dni: "<<31<<endl;
	else if(miesiac==4||miesiac==6||miesiac==9||miesiac==11)
		cout<<"Ilosc dni: "<<30<<endl;
	else if(miesiac==2)
		cout<<"Ilosc dni to "<<28<<", lub "<<29<<" w przypadku roku przestepnego."<<endl;

	return 0;
}
