#include <iostream>
using namespace std;

int main()
{
    int liczba;
    cout << "Wprowadz limit zakresu poszukiwan: " << endl;
    cin >> liczba;
    cout <<"Liczby doskonale z zadanego przedzialu: \n";
    for(int i =1; i<liczba; i++)
    {
        int suma(0);

        for(int j=1; j<i; j++)
        {
            if(i%j==0)
            {
                suma+=j;
            }
        }

        if(suma==i && suma>1)
            cout << suma << endl;
    }

    return 0;
}
